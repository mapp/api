// created_at BIGINT NOT NULL,
// updated_at BIGINT NOT NULL,
// active BOOLEAN NOT NULL,
// alias TEXT NOT NULL,
// passphrase TEXT NOT NULL,
// user UUID NOT NULL,
// score INT NOT NULL,
// taxpayer TEXT,
// data JSONB,
// background JSONB,
// settings JSONB,
// membership JSONB,
// employees JSONB,

// details JSONB NOT NULL, active BOOLEAN NOT NULL, alias TEXT NOT NULL, passphrase TEXT NOT NULL, user UUID NOT NULL, is_supplier NOT NULL, score INT NOT NULL, taxpayer TEXT, data JSONB, background JSONB, settings JSONB, membership JSONB, employees JSONB,

package users

// TABLE COLUMN DETAILS
type Details struct {
	CreatedAt   uint64 `json:"createdAt"`
	LastSession uint64 `json:"lastSession"`
}

type E123Phone struct { // Standard type for any phone numbers
	Prefix      string `json:"prefix"`
	Destination string `json:"destination"`
	Subscriber  string `json:"subscriber"`
}
type Reference struct { // References inidividuals
	Name    string    `json:"name"`
	Surname string    `json:"surname"`
	// Company string    `json:"company"`
	Email   string    `json:"email"`
	Phone   E123Phone `json:"phone"`
	Title   string    `json:"title"`
}
type Locality struct { // The geographical locality
	City    string `json:"city"`
	Road    string `json:"road"`
	Number  string `json:"number"`
	Area    string `json:"area"` // Neighbourhood, borough, etc
	Postal  string `json:"postal"`
	Region  string `json:"region"` // State, province, canton, etc
	Country string `json:"country"`
	Extra   string `json:"extra"`
}
type Address struct { // Every address must contain a receiver or it is just a locality
	Receiver Reference `json:"receiver"`
	Locality Locality  `json:"locality"`
}

type Csc struct { // Código de Segurança do Contribuinte is required to emit NFCs
	Code  string `json:"code"`
	Token string `json:"token"`
}
type CscGroup struct { // The two possible types of CSCs
	Homologation Csc `json:"homologation"`
	Production   Csc `json:"production"`
}
type Sefaz struct { // Accounting fields related to the Secretaria da Fazenda
	Csc                          CscGroup `json:"csc"`
	MunicipalRegistration        string   `json:"municipalRegistration"` // Inscrição estadual
	StateRegistration            string   `json:"stateRegistration"` // Inscrição estadual
	DigitalCertificateExpiration uint64   `json:"digitalCertificateExpiration"`
}
type Enotas struct { // Fields required to run the eNotas
	BusinessId string `json:"businessId"`
}
type Miscellaneous struct {
	Sefaz  Sefaz  `json:"sefaz"`
	Enotas Enotas `json:"enotas"`
}
type Administrative struct {
    Reference Reference `json:"reference"` // Who is the person to be contacted within the company
	Company   string    `json:"company"` // "Razão social" or the legal name which is used by the business in documents
	Byname    string    `json:"byname"` // "Nome fantasia" or the name which the business is known by in the market
}

type Contacts struct {
	Phones []E123Phone `json:"phones"`
}

type Showcase struct {
	Active bool `json:"active"`
}
type Checkout struct {
	Active bool `json:"active"`
}
type Ecommerce struct {
	Active   bool     `json:"active"`
	Showcase Showcase `json:"showcase"`
	Checkout Checkout `json:"checkout"`
}

type Environment struct {
	Currency string `json:"currency"`
	Language string `json:"language"`
}

type Interest struct {
	Compound   bool    `json:"compound"`
	Percentage float32 `json:"percentage"`
}
type Installments struct {
	Active   bool     `json:"active"`
	Custom   bool     `json:"custom"`
	Period   uint8    `json:"period"`
	Interest Interest `json:"interest"`
}

type Shipping struct {
	Active bool    `json:"active"`
	Custom bool    `json:"custom"`
	Fee    float32 `json:"fee"`
	Unit   string  `json:"unit"`
	Ante   float32 `json:"ante"`
}

type Category struct {
	Tag             string   `json:"tag"`
	ReferencedItems []uint64 `json:"referencedItems"`
}

type Employee struct { // Used to store/marshall/unmarshall the employee info from database
	CreatedAt  uint64    `json:"createdAt"`
	Active     bool      `json:"active"`
	Taxpayer   string    `json:"taxpayer"`
	Name       string    `json:"name"`
	Surname    string    `json:"surname"`
	Phone      E123Phone `json:"phone"`
	// Identifier string    `json:"identifier"`
	Passphrase string    `json:"passphrase"`
}
type NonSensitiveEmployee struct { // Used to unmarshall the data from database and send to the page
	CreatedAt  uint64    `json:"createdAt"`
	Active     bool      `json:"active"`
	Taxpayer   string    `json:"taxpayer"`
	Name       string    `json:"name"`
	Surname    string    `json:"surname"`
	Phone      E123Phone `json:"phone"`
}
// TABLE COLUMN EMPLOYEES
type Employees struct {
	Carriers []NonSensitiveEmployee `json:"carriers"`
	Sellers  []NonSensitiveEmployee `json:"sellers"`
}

type EmployeesRegister struct {
	Carriers []Employee `json:"carriers"`
	Sellers  []Employee `json:"sellers"`
}

type Timetable struct { // Integer representing the minutes from midnight
	Opening uint16 `json:"opening"`
	Closing uint16 `json:"closing"`
}
type OperationalDay struct {
	Active   bool      `json:"active"`
	Schedule Timetable `json:"schedule"`
}
type OperationalWeek struct {
	Monday    OperationalDay `json:"monday"`
	Tuesday   OperationalDay `json:"tuesday"`
	Wednesday OperationalDay `json:"wednesday"`
	Thursday  OperationalDay `json:"thursday"`
	Friday    OperationalDay `json:"friday"`
	Saturday  OperationalDay `json:"saturday"`
	Sunday    OperationalDay `json:"sunday"`
}

// TABLE COLUMN DATA
type Data struct {
	Administrative Administrative  `json:"administrative"`
	Locality       Locality        `json:"locality"`
	Contacts       interface{}     `json:"contacts"`
	Ecommerce      Ecommerce       `json:"ecommerce"`
	Environment    Environment     `json:"environment"`
	Installments   Installments    `json:"installments"`
	Shipping       Shipping        `json:"shipping"`
	Categories     []Category      `json:"categories"`
	ServiceWeek    OperationalWeek `json:"serviceWeek"`
	DeliveryWeek   OperationalWeek `json:"deliveryWeek"`
	Miscellaneous  Miscellaneous   `json:"miscellaneous"`
	Addresses      []Address       `json:"addresses"` // Addresses where this user can receive any products purchased
}

type Headers struct { // Warehouse display settings
	Actions           bool `json:"actions"`
	Image             bool `json:"image"`
	Entry             bool `json:"entry"`
	Manufacturer      bool `json:"manufacturer"`
	Model             bool `json:"model"`
	Code              bool `json:"code"`
	Description       bool `json:"description"`
	Category          bool `json:"category"`
	Measure           bool `json:"measure"`
	Cost              bool `json:"cost"`
	Price             bool `json:"price"`
	Markup            bool `json:"markup"`
	Quantity          bool `json:"quantity"`
	AutoDeactivatable bool `json:"autoDeactivatable"`
	Returnable        bool `json:"returnable"`
	Available         bool `json:"available"`
	Discount          bool `json:"discount"`
	CreatedAt         bool `json:"createdAt"`
	UpdatedAt         bool `json:"updatedAt"`
}
type Table struct {Headers Headers `json:"headers"`}
type Items struct {Table Table `json:"table"`}
// TABLE COLUMN SETTINGS
type Settings struct {Items Items `json:"items"`}

// Only allow company to consult/edit information if the consumer explicitly agrees (maybe clicking on a link sent by SMS?)
type Business struct {
    Company   string    `json:"company"`
	Reference Reference `json:"reference"`
	Address   Address   `json:"address"`
}

type Checks struct {
	Homeowner   bool    `json:"homeowner"`   // In case the person owns land (preferably a home)
	Rent        float32 `json:"rent"`        // How much the person pays rent if so
// 0 - Not applicable
// 1 - Married (and not separated)
// 2 - Widowed
// 3 - Separated
// 4 - Divorced
// 5 - Single
	CivilStatus uint8   `json:"civilStatus"` // The status' numbers as shown just above
	GrossIncome float32 `json:"grossIncome"` // Monthly gross income
	// CreditLimit uint64  `json:"creditLimit"` // Credit limit available
	// Occupation  uint8   `json:"occupation"`  // Professional occupation
	ClosingDay  uint8   `json:"closingDay"`  // The preferred day of the month for payments (due dates)
}
// TABLE COLUMN BACKGROUND
type Background struct {
	Checks     Checks      `json:"finance"`
	Notes      []string    `json:"notes"`
	Home       Locality    `json:"home"`
	Billing    Locality    `json:"billing"`
	Nickname   string      `json:"nickname"`
	Workplace  Business    `json:"workplace"`
	References []Reference `json:"references"`
	// UpdatedAt  uint64               `json:"updatedAt"`
	// UpdatedBy  string               `json:"updatedBy"`
}











// Used for authorization process & access information updates
type AuthorizationRequest struct {
	Taxpayer   string `json:"taxpayer"`
	Passphrase string `json:"passphrase"`
}
type AuthorizationResponse struct {
	Token   *string     `json:"token"` // JWT field
	Payload interface{} `json:"payload"`
	Status  uint8       `json:"status"`
}

// Used for the carrier, seller and any other employees authorization process
type EmployeesAuthorizationRequest struct {
	Taxpayer   string  `json:"taxpayer"`
	Alias      string  `json:"alias"`
	Passphrase string  `json:"passphrase"`
}
type EmployeesPassphraseUpdateRequest struct {
	Taxpayer   string  `json:"taxpayer"`
	Alias      string  `json:"alias"`
	Code       string  `json:"code"`
	Passphrase string  `json:"passphrase"`
}



type SupplierAccount struct {
	Data       Data        `json:"data"`
	Employees  Employees   `json:"employees"`
	Settings   Settings    `json:"settings"`
	Membership interface{} `json:"membership"`
}

// Used for registration of new users
type User struct {
	CreatedAt  uint64      `json:"createdAt"`
	UpdatedAt  uint64      `json:"updatedAt"`
	isSupplier bool        `json:"isSupplier"`
	Active     bool        `json:"active"`
	Alias      string      `json:"alias"`
	Passphrase string      `json:"passphrase"`
	User       string      `json:"user"`
	Score      uint16      `json:"score"`
	Taxpayer   string      `json:"taxpayer"`
	Data       Data        `json:"data"`
	Background Background  `json:"background"`
	Settings   Settings    `json:"settings"`
	Membership interface{} `json:"membership"`
	Employees  Employees   `json:"employees"`
}