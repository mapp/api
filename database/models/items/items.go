// Minified table configuration
// item TEXT NOT NULL, active BOOLEAN NOT NULL, supplier TEXT NOT NULL, entry BIGINT NOT NULL, data JSONB, created_at BIGINT NOT NULL, updated_at BIGINT NOT NULL

package items

type Media struct {
	Images []string `json:"images"`
}

type Origin struct {
	Manufacturer string `json:"manufacturer"`
	Model        string `json:"model"`
}

type Groups struct {
	Category string `json:"category"`
}

type Sudden struct {
	Custom bool    `json:"custom"`
	Value  float32 `json:"value"`
}
type Costs struct {
	Production float32 `json:"production"`
	Variable   float32 `json:"variable"`
	Fixed      float32 `json:"fixed"`
	Sudden     Sudden  `json:"sudden"`
}
type PriceVariables struct {
	UpdatedAt uint64  `json:"updatedAt"`
	Tag       string  `json:"tag"`
	Value     float64 `json:"value"`
}
type Manual struct {
	Active    bool             `json:"active"`
	Formula   string           `json:"formula"`
	Variables []PriceVariables `json:"variables"`
}
type Automatic struct {
	Active bool `json:"active"`
}
type Price struct { // To calculate for the online store, manually
	Manual    Manual    `json:"manual"`
	Automatic Automatic `json:"automatic"`
}
type ShelfLife struct {
	Active     bool   `json:"active"`
	ProducedAt uint64 `json:"producedAt"`
	ExpiresAt  uint64 `json:"expiresAt"`
}
type Batch struct { // Can not be just modified, just overwritten whenever the supplier updates the item quantity
	Series    string    `json:"series"`    // A base62 encoded (int)serial number (that iterates) (in case the order property Available is true, on every new order instead of just on every new batch)
	ShelfLife ShelfLife `json:"shelfLife"` // In case it is not set by the user, it is null (saving precious bytes!)
}
type Control struct { // Information set freely by the supplier
	Code              string  `json:"code"`
	Description       string  `json:"description"`
	Summary           string  `json:"summary"`
	Measure           string  `json:"measure"`
	Costs             Costs   `json:"costs"`
	Price             Price   `json:"price"`
	Markup            float32 `json:"markup"`
	Quantity          float64 `json:"quantity"`
	Batch             Batch   `json:"batch"`
	Fractionable      bool    `json:"fractionable"`
	AutoDeactivatable bool    `json:"autoDeactivatable"`
	Returnable        bool    `json:"returnable"`
	Available         bool    `json:"available"`
}

type Discount struct {
	Custom     bool    `json:"custom"`
	Quota      float64 `json:"quota"`
	Percentage float32 `json:"percentage"`
	// InitialDate uint64   `json:"initialDate"`
	// EndingDate  uint64   `json:"endDate"`
}

type Measurement struct {
	Type  string  `json:"type"`
	Value float32 `json:"value"`
	Unit  string  `json:"unit"`
}
type Logistics struct {
	Dimensions []Measurement `json:"dimensions"`
	Stackable  bool          `json:"stackable"`
}

type Ipi struct {
	Cst          string `json:"cst"`
	FramingCode  string `json:"framingCode"`
	FramingClass string `json:"framingClass"`
	Rate         string `json:"rate"`
	ControlCode  string `json:"controlCode"`
	ProducerCnpj string `json:"producerCnpj"`
}
type PisOrCofins struct {
	Cst  string `json:"cst"`
	Rate string `json:"rate"`
}
type Pis struct {
	In  PisOrCofins `json:"in"`
	Out PisOrCofins `json:"out"`
}
type Cofins struct {
	In  PisOrCofins `json:"in"`
	Out PisOrCofins `json:"out"`
}
type St struct {
	Mva                       string `json:"mva"`
	Rate                      string `json:"rate"`
	CalculationBasis          string `json:"calculationBasis"`
	CalculationBasisModality  string `json:"calculationBasisModality"`
	CalculationBasisReduction string `json:"calculationBasisReduction"`
}
type Icms struct {
	Cst                       string `json:"cst"`
	Origin                    int8   `json:"origin"`
	Rate                      string `json:"rate"`
	CalculationBasis          string `json:"calculationBasis"`
	CalculationBasisModality  string `json:"calculationBasisModality"`
	CalculationBasisReduction string `json:"calculationBasisReduction"`
	St                        St     `json:"st"`
}

type BrazilianItemAccounting struct { // Accounting
	Ncm    string `json:"ncm"`
	Cest   string `json:"cest"`
	Cfop   string `json:"cfop"`
	Icms   Icms   `json:"icms"`
	Pis    Pis    `json:"pis"`
	Cofins Cofins `json:"cofins"`
	Ipi    Ipi    `json:"ipi"`
}

type Accounting struct {
	Active bool                    `json:"active"`
	Rules  BrazilianItemAccounting `json:"rules"` // Null in case the the store does not want to do accounting/Will vary from country to country
}

type Data struct {
	Media      Media      `json:"media"`
	Origin     Origin     `json:"origin"`
	Groups     Groups     `json:"groups"`
	Control    Control    `json:"control"`
	Discount   Discount   `json:"discount"`
	Logistics  Logistics  `json:"logistics"`
	Accounting Accounting `json:"accounting"` // Everything related to taxation, null in case the the store does not want to do accounting. Varies from country to country
}

type Item struct {
	Data   Data   `json:"data"`
	Entry  uint64 `json:"entry"`
	Active bool   `json:"active"`
	// Tangible  bool    `json:"tangible"`
	CreatedAt uint64 `json:"createdAt"`
	UpdatedAt uint64 `json:"updatedAt"`
}
