package relationships

import (
	// Standard packages
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"

	// Remote packages
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"

	// Local packages
	"api/database/models/orders"
	"api/database/models/relationships"
	"api/database/models/users"
	pg "api/database/postgres/connection"
	"api/helpers/functions/concat"
	"api/helpers/functions/logger"
	env "api/helpers/variables/environment"
)

// Retrieves all orders from a given supplier uuid
func SelectSupplierRelationships (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)
	// Executes the database query

	list := []orders.SupplierOrder{}
	rows, _ := pg.Pool.Query(
		context.Background(),
		// "SELECT record, created_at, updated_at, data FROM orders WHERE "user" = $1",
		// user)
		`SELECT
			users.data, orders.record, orders.created_at, orders.updated_at, orders.data
		FROM
			users
		INNER JOIN
			orders
		ON
			users.user = orders.customer
		WHERE
			"user" = $1`,
		user)

	for rows.Next() {
		var order orders.SupplierOrder
		err := rows.Scan(&order.CustomerData, &order.Record, &order.CreatedAt, &order.UpdatedAt, &order.OrderData)
		if err != nil {
			logger.Error.Println(err)
			return c.SendStatus(500)
		}
		// Empty array initializations
		if order.OrderData.Delivery.Carriers == nil {
			order.OrderData.Delivery.Carriers = []orders.Carrier{}
		}
		// if order.OrderData.Delivery.Geodata == nil {
		// 	order.OrderData.Delivery.Geodata = []orders.Geoframe{}
		// }
		list = append(list, order)
	}

	// Returns the retrieved orders
	return c.JSON(list)
}

func SelectSupplierOrder (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	record := c.Params("record")
	// Executes the database query
	var order orders.SupplierOrder
	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT
			users.data, orders.record, orders.created_at, orders.updated_at, orders.data
		FROM
			users
		INNER JOIN
			orders
		ON
			users.user = orders.customer
		WHERE
			"user" = $1
		AND
			record = $2`,
		user, record).Scan(&order.CustomerData, &order.Record, &order.CreatedAt, &order.UpdatedAt, &order.OrderData)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(500)
	}
	// Empty array initializations
	if order.OrderData.Delivery.Carriers == nil {
		order.OrderData.Delivery.Carriers = []orders.Carrier{}
	}
	// if order.OrderData.Delivery.Geodata == nil {
	// 	order.OrderData.Delivery.Geodata = []orders.Geoframe{}
	// }
	// Returns the retrieved data
	return c.JSON(order)
}

// Updates the supplier order (status)
func UpdateSupplierOrderData (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	// token := c.Locals("authorization").(*jwt.Token)
	// claims := token.Claims.(jwt.MapClaims)
	// user := claims["user"].(string)
	// Parses the body data
	var order orders.Order // This struct stores the entire JSON
	if err := c.BodyParser(&order); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// order.Supplier = supplier
	// Updates the Unix timestamp
	order.UpdatedAt = uint64(time.Now().Unix())

	// Executes the database query
	if _, err := pg.Pool.Exec(
		context.Background(),
		`UPDATE
			orders
		SET
			updated_at = $1, data = $2
		WHERE
			"user" = $3
		AND
			record = $4`,
		order.UpdatedAt, order.Data, order.Supplier, order.Record)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	return c.JSON(fiber.Map{
		"updatedAt": order.UpdatedAt,
	})
}



func sortBrazilianInvoiceType (model uint64) string {
	if (model == 1) {
		return "nf-e"
	} else if (model == 2) {
		return "ct-e"
	} else if (model == 3) {
		return "nfs-e"
	}
	return "nfc-e"
}
func InsertSupplierOrderInvoice (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	var invoiceData interface{}
	if err := c.BodyParser(&invoiceData); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	orderRecord := c.Params("record")
	invoiceModel, err := strconv.ParseUint(c.Params("model"), 10, 64)
	var businessId string
	err = pg.Pool.QueryRow(
		context.Background(),
		`SELECT
			data->administrative->miscellaneous->enotas->businessId
		FROM
			users
		WHERE
			"user" = $1`,
		user).Scan(&businessId)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	payload := strings.NewReader(invoiceData.(string))
	url := concat.Join("https://api.enotasgw.com.br/v2/empresas/", businessId, "/", sortBrazilianInvoiceType(invoiceModel))
	req, _ := http.NewRequest("POST", url, payload)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", concat.Join("Basic ", env.EnotasApiToken))

	res, _ := http.DefaultClient.Do(req)
	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))
	// HOW TO PROCESS ALL THAT STUFF, NEED TO DO IT
	details := "" // This is the response from eNotas, whatever it is

	invoiceRecord := uuid.New()
	createdAt := uint64(time.Now().Unix())
	updatedAt := createdAt

	if _, err := pg.Pool.Exec(
		context.Background(),
		"INSERT INTO invoices (record, model, order, status, details, created_at, updated_at) VALUES ($1, $2, $3, $4, $5, $6, $7)",
		invoiceRecord, invoiceModel, orderRecord, 0, details, createdAt, updatedAt)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	// return c.SendStatus(200)
	return c.SendString(string(body))
}

func SelectRelationships (c *fiber.Ctx) error {
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}

	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	list := []relationships.Relationship{}
	
	rows, _ := pg.Pool.Query(
		context.Background(),
		`SELECT target, reference, relation, payables, receivables FROM relationships WHERE reference = $1 OR target = $1`,
		user)

	for rows.Next() {
		var relationship relationships.Relationship
		var target string
		var reference string

		err := rows.Scan(&target, &reference, &relationship.Relation, &relationship.Situation.Payables, &relationship.Situation.Receivables)
		if err != nil {
			logger.Error.Println(err)
			return c.SendStatus(500)
		}

		trueTarget := target
		truePayables := relationship.Situation.Payables
		trueReceivables := relationship.Situation.Receivables

		if user == target {
			trueTarget= reference
			relationship.Situation.Payables = trueReceivables
			relationship.Situation.Receivables = truePayables	
		}

		err = pg.Pool.QueryRow(
			context.Background(),
			`SELECT 
				data->'administrative'->>'company',
				data->'administrative'->>'byname',
				taxpayer,
				data->'administrative'->'reference'->>'name',
				data->'administrative'->'reference'->>'surname'
			FROM 
				users 
			WHERE 
				"user" = $1`,
				trueTarget).Scan(&relationship.Company, &relationship.Byname, &relationship.Taxpayer, &relationship.Name, &relationship.Surname)
		if err != nil {
			logger.Error.Println(err)
			return c.SendStatus(400)
		}
		
		list = append(list, relationship)
	}
	return c.JSON(list)
}

func SelectRelationshipsTaxpayer (c *fiber.Ctx) error {
	taxpayer := c.Params("identifier"); if taxpayer == "" {
		return c.SendStatus(400)
	}

	var relationship relationships.Relationship

		err := pg.Pool.QueryRow(
			context.Background(),
			`SELECT 
				data->'administrative'->>'company',
				data->'administrative'->>'byname',
				data->'administrative'->'reference'->>'name',
				data->'administrative'->'reference'->>'surname'
			FROM 
				users 
			WHERE 
				taxpayer = $1`,
				taxpayer).Scan(&relationship.Company, &relationship.Byname, &relationship.Name, &relationship.Surname)
		if err != nil {
			logger.Error.Println(err)
			return c.SendStatus(400)
		}

	return c.JSON(relationship)
}

type newRelationshipWithExistedUser struct {
	Relation uint8 `json:"relation"`
	Situation relationships.Situation `json:"situation"`
}
func InsertExistedUserAsRelationship (c *fiber.Ctx) error {
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}

	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	taxpayer := c.Params("identifier"); if taxpayer == "" {
		return c.SendStatus(400)
	}

	var payload newRelationshipWithExistedUser // This struct stores the entire JSON
	if err := c.BodyParser(&payload); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	var target string

	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT "user" FROM users WHERE taxpayer = $1`,
		taxpayer).Scan(&target)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(404)
	}

	createdAt := uint64(time.Now().Unix())
	updatedAt := createdAt

	if _, err := pg.Pool.Exec(
		context.Background(),
		`INSERT INTO
			relationships (created_at, updated_at, reference, target, relation, payables, receivables)
		VALUES
			($1, $2, $3, $4, $5, $6, $7)`,
		createdAt, updatedAt, user, target, payload.Relation, payload.Situation.Payables, payload.Situation.Receivables )
	err != nil {
		logger.Error.Println(err);
		return c.SendStatus(400)
	}

	return c.SendStatus(200)

}

type RelationshipPaymentsPayables struct {
	Taxpayer string `json:"taxpayer"`
	Payables []relationships.Accounts `json:"payables"`
}
func InsertRelationshipPayables (c * fiber.Ctx) error  {

	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}

	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	var payload RelationshipPaymentsPayables

	if err := c.BodyParser(&payload); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	var relationshipTarget string

	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT "user" FROM users WHERE taxpayer = $1`,
		payload.Taxpayer).Scan(&relationshipTarget)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(404)
	}

	var target string //relationship target
	var reference string 
	var payables []relationships.Accounts
	var receivables []relationships.Accounts

	// Executes the database query
	err2:= pg.Pool.QueryRow(
		context.Background(),
		`SELECT target, reference, payables, receivables FROM relationships WHERE (reference = $1 AND target = $2) OR (reference = $2 AND target = $1)`,
		user, relationshipTarget).Scan(&target, &reference, &payables, &receivables)
	if err2 != nil {
		logger.Error.Println(err2)
		return c.SendStatus(404)
	}

		payables = append(payables, payload.Payables...)
		if _, err := pg.Pool.Exec(
			context.Background(),
			`UPDATE relationships SET payables = $1 WHERE reference = $2 AND target = $3`,
			payables, reference, target)
		err != nil {
			logger.Error.Println(err)
			return c.SendStatus(400)
		}


	// Returns a success status
	return c.SendStatus(200)

}

type RelationshipPaymentsReceivables struct {
	Taxpayer string `json:"taxpayer"`
	Receivables []relationships.Accounts `json:"receivables"`
}
func InsertRelationshipReceivables (c * fiber.Ctx) error  {
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}

	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	var payload RelationshipPaymentsReceivables

	if err := c.BodyParser(&payload); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	logger.Info.Println(payload)

	var relationshipTarget string

	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT "user" FROM users WHERE taxpayer = $1`,
		payload.Taxpayer).Scan(&relationshipTarget)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(404)
	}

	var target string
	var reference string
	var payables []relationships.Accounts
	var receivables []relationships.Accounts

	err2:= pg.Pool.QueryRow(
		context.Background(),
		`SELECT target, reference, payables, receivables FROM relationships WHERE (reference = $1 AND target = $2) OR (reference = $2 AND target = $1)`,
		user, relationshipTarget).Scan(&target, &reference, &payables, &receivables)
	if err2 != nil {
		logger.Error.Println(err2)
		return c.SendStatus(404)
	}

	

		receivables = append(receivables, payload.Receivables...)

		if _, err := pg.Pool.Exec(
			context.Background(),
			`UPDATE relationships SET receivables = $1 WHERE reference = $2 AND target = $3`,
			receivables, reference, target)
		err != nil {
			logger.Error.Println(err)
			return c.SendStatus(400)
		}

	// Returns a success status
	return c.JSON(200)
}

func DeleteRelationshipPayables (c * fiber.Ctx) error  {

	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}

	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	var payload RelationshipPaymentsPayables

	if err := c.BodyParser(&payload); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	var relationshipTarget string

	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT "user" FROM users WHERE taxpayer = $1`,
		payload.Taxpayer).Scan(&relationshipTarget)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(404)
	}

	var target string //relationship target
	var reference string 
	var payables []relationships.Accounts

	// Executes the database query
	err2:= pg.Pool.QueryRow(
		context.Background(),
		`SELECT target, reference, payables FROM relationships WHERE (reference = $1 AND target = $2) OR (reference = $2 AND target = $1)`,
		user, relationshipTarget).Scan(&target, &reference, &payables)
	if err2 != nil {
		logger.Error.Println(err2)
		return c.SendStatus(404)
	}

	payables = payload.Payables
	if _, err := pg.Pool.Exec(
		context.Background(),
		`UPDATE relationships SET payables = $1 WHERE reference = $2 AND target = $3`,
		payables, reference, target)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// Returns a success status
	return c.SendStatus(200)
}

func DeleteRelationshipReceivables (c * fiber.Ctx) error  {

	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}

	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	var payload RelationshipPaymentsReceivables

	if err := c.BodyParser(&payload); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	var relationshipTarget string

	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT "user" FROM users WHERE taxpayer = $1`,
		payload.Taxpayer).Scan(&relationshipTarget)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(404)
	}

	var target string //relationship target
	var reference string 
	var receivables []relationships.Accounts

	// Executes the database query
	err2:= pg.Pool.QueryRow(
		context.Background(),
		`SELECT target, reference, receivables FROM relationships WHERE (reference = $1 AND target = $2) OR (reference = $2 AND target = $1)`,
		user, relationshipTarget).Scan(&target, &reference, &receivables)
	if err2 != nil {
		logger.Error.Println(err2)
		return c.SendStatus(404)
	}

	receivables = payload.Receivables
	if _, err := pg.Pool.Exec(
		context.Background(),
		`UPDATE relationships SET receivables = $1 WHERE reference = $2 AND target = $3`,
		receivables, reference, target)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// Returns a success status
	return c.SendStatus(200)
}

type RelationshipDataBackgroud struct{
	Taxpayer   string      		 `json:"taxpayer"`
	Data       users.Data   	 `json:"data"`     
	Background users.Background  `json:"background"`
}

func UpdateRelationship (c *fiber.Ctx) error {

	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	
	var dataBackground RelationshipDataBackgroud

	if err := c.BodyParser(&dataBackground); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	timestamp := strconv.FormatUint(uint64(time.Now().Unix()), 10)
	

		err := pg.Pool.QueryRow(
			context.Background(),
			`UPDATE
				users
			SET
				updated_at = $1,
				background = $2,
				data = $3
			WHERE
				taxpayer = $4`,

				timestamp, dataBackground.Background, dataBackground.Data, dataBackground.Taxpayer)

		if err != nil {
			logger.Error.Println(err)
			return c.SendStatus(400)
		}

	return c.JSON(dataBackground)
}