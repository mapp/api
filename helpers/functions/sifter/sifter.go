package sifter

import (
	"time"
	"strings"
	"strconv"
	"context"

	// Local packages
	"api/helpers/functions/concat"
	"api/helpers/functions/randomizer"
	redis "api/database/redis/connection"
	"api/services/gatewayApi"

	"api/database/models/users"

"fmt"
)

var (
	uppercaseAlpha              = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	availableDestinations       = []string {"38", "34"}
	availableDestinationsLength = len(availableDestinations)
)

// For beta, just customers from the area code 38 are allowed to use
// Brazil mobile format is 55 XX 9 XXXX XXXX
func SetCustomerStatusOnKey (phone users.E123Phone) uint8 {
    for i := 0; i < availableDestinationsLength; i++ {
		if strings.Compare(phone.Destination, availableDestinations[i]) == 0 {
			return 0 // As such, the user is allowed to use the platform
		}
    }
    return 1
}

// func SendCode(phone string, prefix string) error {
func SendCode(phone users.E123Phone) error {
	// Generates the code for verification
	code := randomizer.GenerateRandomString(4, uppercaseAlpha)
	fmt.Println(code)
	message := concat.Join("Materiapp: Use a chave ", code)
	mobile := concat.Join(phone.Prefix, phone.Destination, phone.Subscriber)

	// Makes the client request to GatewayAPI service
	number, err := strconv.ParseUint(string(mobile), 10, 64); if err != nil { // Using "if" before number will make it usable within its score only
	    return err
	}
	if err := gatewayApi.SendSms("Materiapp", message, number); err != nil {
		return err
	}

	// Stores the data in the Redis database
	client := redis.NewClient()
	ctx := context.Background()
	expiresIn := 10 * 60 * time.Second // Valid for 10 minutes
	// Call set with a "key" (mobile) and a "value" (code)
	if err := client.Set(ctx, mobile, code, expiresIn).Err(); err != nil {
	    return err
	}
	return nil
}

func VerifyCode(phone users.E123Phone, code string) (bool, error) {
	// Stores the data in the Redis database
	client := redis.NewClient()
	ctx := context.Background()
	mobile := concat.Join(phone.Prefix, phone.Destination, phone.Subscriber)
	storedCode, err := client.Get(ctx, mobile).Result()
	if err != nil {
	    return false, err
	}

	// Compares the code sent (stored in Redis) with the code typed by the user
	if strings.Compare(storedCode, code) == 0 {
		return true, nil
	} else {
		return false, nil
	}
}