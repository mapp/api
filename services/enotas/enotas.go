package enotas

import (
	"api/helpers/functions/logger"
	env "api/helpers/variables/environment"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	

)

// Auxiliar structs
type Qrcode struct {
	Conteudo string `json:"conteudo"`
}

type Protocolo struct {
	Numero      string `json:"numero"`
	DigestValue string `json:"digestValue"`
}

// Parse the response
type EmissionResponse struct {
	Id                         string    `json:"id"`
	AmbienteEmissao            string    `json:"ambienteEmissao"`
	Status                     string    `json:"status"`
	Numero                     string    `json:"numero"`
	Serie                      string    `json:"serie"`
	ChaveAcesso                string    `json:"chaveAcesso"`
	LinkDownloadXml            string    `json:"linkDownloadXml"`
	LinkDanfe                  string    `json:"linkDanfe"`
	LinkConsultaPorChaveAcesso string    `json:"linkConsultaPorChaveAcesso"`
	Qrcode                     Qrcode    `json:"qrcode"`
	Protocolo                  Protocolo `json:"protocolo"`
	InformacoesAdicionais      string    `json:"informacoesAdicionais"`
}

type BusinessId struct {
	EmpresaId string `json:"empresaId"`
}

// include a new business into enotas
func EnotasNewBusiness(payload interface{}) (string, error) {

	//post body
	postBody, _ := json.Marshal(payload)

	// url
	url := "https://api.enotasgw.com.br/v2/empresas"

	// authorization
	authorization := fmt.Sprintf("Basic %s", env.EnotasApiToken)

	//request
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(postBody))
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", authorization)
	// failed request
	if err != nil {
		logger.Error.Println("failed request")
	}

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		logger.Error.Println("error client do")
	}

	defer req.Body.Close()

	response := &BusinessId{}
	if err := json.NewDecoder(res.Body).Decode(response); err != nil {
		return response.EmpresaId, err
	}

	return response.EmpresaId, nil
}

// Emission of a NFC-e
func NfceEmission(payload interface{}, businessId string) (*EmissionResponse, error) {

	//post body
	postBody, _ := json.Marshal(payload)

	// authorization
	authorization := fmt.Sprintf("Basic %s", env.EnotasApiToken)

	//url
	url := fmt.Sprintf(`https://api.enotasgw.com.br/v2/empresas/%s/nfc-e`, businessId)

	// eNotas API request
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(postBody))

	// headers
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", authorization)
	// failed request
	if err != nil {
		logger.Error.Println("failed request")
	}

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		logger.Error.Println("error")
	}

	defer req.Body.Close()

	response := &EmissionResponse{}
	if err := json.NewDecoder(res.Body).Decode(response); err != nil {
		return response, err
	}

	return response, nil
}

// Cancel a NFC-e
func CancelNfce(NfceId string, businessId string) error {

	// authorization
	authorization := fmt.Sprintf("Basic %s", env.EnotasApiToken)

	//url
	url := fmt.Sprintf(`https://api.enotasgw.com.br/v2/empresas/%s/nfc-e/%s`, businessId, NfceId)

	// eNotas API request
	req, err := http.NewRequest("DELETE", url, nil)

	// headers
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Authorization", authorization)
	// failed request
	if err != nil {
		logger.Error.Println("failed request")
	}

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		logger.Error.Println("error")
	}

	defer req.Body.Close()

	var response interface{}
	if err := json.NewDecoder(res.Body).Decode(response); err != nil {
		return err
	}

	return nil
}

/* --------------  Nfe -------------- */
//url

// Emission of a NFC-e
func NfeEmission(payload interface{}, businessId string) (interface{}, error) {
	//post body
	postBody, _ := json.Marshal(payload)

	// authorization
	authorization := fmt.Sprintf("Basic %s", env.EnotasApiToken)

	//url
	url := fmt.Sprintf(`https://api.enotasgw.com.br/v2/empresas/%s/nfc-e`, businessId)

	// eNotas API request
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(postBody))

	// headers
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", authorization)
	// failed request
	if err != nil {
		logger.Error.Println("failed request")
	}

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		logger.Error.Println("error")
	}

	defer req.Body.Close()

	var response interface{}
	if err := json.NewDecoder(res.Body).Decode(response); err != nil {
		return response, err
	}

	return response, nil

}

// Cancel a NFC-e
func CancelNfe(NfeId string, businessId string) (interface{}, error) {

	// authorization
	authorization := fmt.Sprintf("Basic %s", env.EnotasApiToken)

	//url
	url := fmt.Sprintf(`https://api.enotasgw.com.br/v2/empresas/%s/nf-e/%s`, businessId, NfeId)

	// eNotas API request
	req, err := http.NewRequest("DELETE", url, nil)

	// headers
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Authorization", authorization)
	// failed request
	if err != nil {
		logger.Error.Println("failed request")
	}

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		logger.Error.Println("error")
	}

	defer req.Body.Close()

	var response interface{}
	if err := json.NewDecoder(res.Body).Decode(response); err != nil {
		return response, err
	}

	return response, nil
}

func EnotasLinkLogo(businessId string, data multipart.FileHeader) (*http.Response, error) {

	// authorization
	authorization := fmt.Sprintf("Basic %s", env.EnotasApiToken)

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	fw, err := writer.CreateFormFile("logotipo", data.Filename)
	if err != nil {
		logger.Error.Println("Error", err)
	}
	fileM, err := data.Open()
	if err != nil {
		logger.Error.Println("Error", err)
	}
	io.Copy(fw, fileM)
	writer.Close()

	url := fmt.Sprintf(`https://api.enotasgw.com.br/v2/empresas/%s/logo`, businessId)

	req, err := http.NewRequest("POST", url, bytes.NewReader(body.Bytes()))
	if err != nil {
		logger.Error.Println("Error", err)
	}

	// headers
	req.Header.Add("Content-Type", writer.FormDataContentType())
	req.Header.Add("Accept", "NaN")
	req.Header.Add("Content-Length", fmt.Sprintf("%d", body.Len()))
	req.Header.Add("Authorization", authorization)
	req.Header.Set("Accept-Encoding", "")

	client := &http.Client{}
	res, err := client.Do(req)
	if res.StatusCode != http.StatusOK {
		defer req.Body.Close()
		return res, err
	}

	defer req.Body.Close()
	return res, err
}

func EnotasLinkCertificate(businessId string, data multipart.FileHeader, password string) (*http.Response, error) {

	// authorization
	authorization := fmt.Sprintf("Basic %s", env.EnotasApiToken)

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	_ = writer.WriteField("senha", password)
	fw, err := writer.CreateFormFile("arquivo", data.Filename)
	if err != nil {
		logger.Error.Println("Error", err)
	}
	fileM, err := data.Open()
	if err != nil {
		logger.Error.Println("Error", err)
	}
	io.Copy(fw, fileM)
	writer.Close()

	url := fmt.Sprintf(`https://api.enotasgw.com.br/v2/empresas/%s/certificadoDigital`, businessId)

	req, err := http.NewRequest("POST", url, bytes.NewReader(body.Bytes()))
	if err != nil {
		logger.Error.Println("Error", err)
	}

	// headers
	req.Header.Add("Content-Type", writer.FormDataContentType())
	req.Header.Add("Accept", writer.FormDataContentType())
	req.Header.Add("Content-Length", fmt.Sprintf("%d", body.Len()))
	req.Header.Add("Authorization", authorization)

	client := &http.Client{}
	res, err := client.Do(req)
	if res.StatusCode != http.StatusOK {
		defer req.Body.Close()
		return res, err
	}

	defer req.Body.Close()
	return res, err
}

func EnotasSearchNfe(businessId string, nfeId string) (*http.Response, error) {

	// authorization
	authorization := fmt.Sprintf("Basic %s", env.EnotasApiToken)

	url := fmt.Sprintf(`https://api2.enotasgw.com.br/v3/empresas/2D16D146-971A-4CA2-AEDF-6A514B4A0700/nota-tomada/nf-e/consulta/sem-manifestacao/31210905749654000136550010000101671210929195`)

	req, err := http.NewRequest("POST", url, nil)
	if err != nil {
		logger.Error.Println("Error", err)
	}

	// headers
	req.Header.Add("Accept", "NaN")
	req.Header.Add("Authorization", authorization)

	client := &http.Client{}
	res, err := client.Do(req)
	if res.StatusCode != http.StatusOK {
		defer req.Body.Close()
		return res, err
	}

	defer req.Body.Close()
	return res, err
}
