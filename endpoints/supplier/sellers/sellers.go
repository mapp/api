package seller

import (
	// Standard packages
	"context"
	"time"

	// // Remote packages
	"github.com/alexedwards/argon2id"
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"

	// // Local packages
	"api/helpers/functions/logger"
	"api/helpers/functions/sifter"
	"api/helpers/functions/concat"
	env "api/helpers/variables/environment"

	// "api/helpers/functions/concat"
	"api/database/models/users"
	pg "api/database/postgres/connection"
	// "api/models/response"
)

type Seller struct {
	Data  users.Employee `json:"data"`
	Alias string         `json:"alias"`
}

func AuthenticateSeller(c *fiber.Ctx) error {
	var seller users.EmployeesAuthorizationRequest
	if err := c.BodyParser(&seller); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// Executes the database query to get the supplier number
	var supplier string
	var account Seller

	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT
			"user",
			(
				SELECT * FROM
					jsonb_array_elements(employees->'sellers')
				AS
					elem
				WHERE
					(elem->>'taxpayer') = $1
			)
		AS
			carrier
		FROM
			users
		WHERE
			alias = $2
		AND EXISTS
		(
			SELECT 1 FROM
				jsonb_array_elements(employees->'sellers')
			AS
				elem
			WHERE
				(elem->>'taxpayer') = $1
		)`,
		seller.Taxpayer, seller.Alias).Scan(&supplier, &account.Data)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// Compares the passphrase received by the login page to the passphrase hash stored in the
	// carrier's database entry, continuing to query the orders from it if all good or returning
	// an error if passphrases don not match each other
	match, err := argon2id.ComparePasswordAndHash(seller.Passphrase, account.Data.Passphrase)
	if err != nil { // HASH == "" IS A TEMPORARY MEASURE!
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	if match {
		// Creates the authentication token for the carrier agent to use the platform
		token := jwt.New(jwt.SigningMethodHS256)
		// Set claims
		claims := token.Claims.(jwt.MapClaims)
		claims["user"] = supplier
		claims["taxpayer"] = seller.Taxpayer
		claims["exp"] = time.Now().Add(time.Hour * 72).Unix()
		// Generate encoded token and send it as response
		encodedToken, err := token.SignedString(env.JwtSigningKey)
		if err != nil {
			logger.Error.Println(err)
			return c.SendStatus(500)
		}

		var response users.AuthorizationResponse
		account.Alias = seller.Alias
		response.Token = &encodedToken
		response.Payload = &account
		return c.JSON(response)
	} else {
		return c.SendStatus(404)
	}
}

// Parses the payload struct data, querying the seller (taxpayer), from the user's (alias) employee column,
// verifies the code received with the one previously stored on Redis using the phone as the key and given
// the codes matched each other, the current seller's passphrase hash is replaced by the Argon2fied result of the payload's passphrase field
func UpdateSellerPassphrase(c *fiber.Ctx) error {
	var payload users.EmployeesPassphraseUpdateRequest

	if err := c.BodyParser(&payload); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	var supplier string
	var account Seller // Seller account data

	// Executes the database query to retrieve the
	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT
			"user",
			(
				SELECT * FROM
					jsonb_array_elements(employees->'sellers')
				AS
					elem
				WHERE
					(elem->>'taxpayer') = $1
			)
		AS
			carrier
		FROM
			users
		WHERE
			alias = $2
		AND EXISTS
		(
			SELECT 1 FROM
				jsonb_array_elements(employees->'sellers')
			AS
				elem
			WHERE
				(elem->>'taxpayer') = $1
		)`,
		payload.Taxpayer, payload.Alias).Scan(&supplier, &account.Data)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(404)
	}

	// Code verification
	isVerified, err := sifter.VerifyCode(account.Data.Phone, payload.Code)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	if !isVerified {
		return c.SendStatus(404)
	}

	hash, err := argon2id.CreateHash(payload.Passphrase, argon2id.DefaultParams)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	updatedAt := uint64(time.Now().Unix())

	// Executes the database query
	if _, err := pg.Pool.Exec(
		context.Background(),
		// https://stackoverflow.com/questions/66015250/update-object-field-of-element-in-array-jsonb-with-postgres
		`WITH j AS
		(
			SELECT
				('{sellers,'||idx-1||',passphrase}')::text[]
			AS
				path, j
			FROM 
				users 
			CROSS JOIN
				JSONB_ARRAY_ELEMENTS(employees->'sellers') 
			WITH ORDINALITY 
				arr(j,idx)
			WHERE 
				j->>'taxpayer'= $1
		)
		UPDATE
			users
		SET 
			employees = JSONB_SET(employees,j.path, to_jsonb($2::text),false), updated_at= $3
		FROM 
			j
		WHERE 
			alias = $4`,
		payload.Taxpayer, hash, updatedAt, payload.Alias); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	return c.SendStatus(200)
}

type verifyCodeByTaxpayerAndAlias struct {
	Taxpayer string `json:"taxpayer"`
	Alias    string `json:"alias"`
}

func VerifyUserPhoneByTaxpayer(c *fiber.Ctx) error {
	var payload verifyCodeByTaxpayerAndAlias
	var employee users.Employee

	if err := c.BodyParser(&payload); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT
		(
			SELECT * FROM
				jsonb_array_elements(employees->'sellers')
			AS
				elem
			WHERE
				(elem->>'taxpayer') = $1
		)
	FROM
		users
	WHERE
		alias = $2
	`,
		payload.Taxpayer, payload.Alias).Scan(&employee)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(404)
	}

	if err := sifter.SendCode(employee.Phone); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	return c.SendStatus(200)
}

func SelectCustomerAddresses(c *fiber.Ctx) error {
	// Customer key & identifier parse attempt being empty causes panic
	field := c.Params("identifierField")
	value := c.Params("identifierValue")
	if field == "" || value == "" {
		return c.SendStatus(400)
	}

	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	userReference := claims["user"].(string)
	// Executes query as a security layer to make sure whoever is accessing this resource got a valid token
	var verified bool
	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT EXISTS(SELECT 1 FROM users WHERE "user" = $1)`,
		userReference).Scan(&verified)
	if err != nil || !verified {
		logger.Error.Println(err)
		return c.SendStatus(500)
	}

	var userTarget, name, surname string
	var phone users.E123Phone
	var addresses []users.Address
	// Executes the database query
	sql := concat.Join(`
		SELECT
			"user",
			data->'administrative'->'reference'->>'name',
			data->'administrative'->'reference'->>'surname',
			data->'administrative'->'reference'->'phone',
			data->'addresses'
		FROM
			users
		WHERE `, field, ` = $1`)
	err = pg.Pool.QueryRow(
		context.Background(),
		sql,
		value).Scan(&userTarget, &name, &surname, &phone, &addresses)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(500)
	}
	// Empty array initializations
	if addresses == nil {
		addresses = []users.Address{}
	}
	// Create token for the seller to submit the order on behalf of the customer therefore the order will belong to this customer history
	token = jwt.New(jwt.SigningMethodHS256)
	// Set claims
	claims = token.Claims.(jwt.MapClaims)
	claims["user"] = userTarget
	claims["exp"] = time.Now().Add(time.Minute * 20).Unix() // Checkout session is valid for 20 minutes
	encodedToken, err := token.SignedString(env.JwtSigningKey)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(500)
	}

	var r users.AuthorizationResponse
	r.Payload = fiber.Map{
		"name":      name,
		"surname":   surname,
		"phone":     phone,
		"addresses": addresses,
	}
	r.Token = &encodedToken
	// Returns the retrieved data
	return c.JSON(r)
}

func SelectSupplierCarriers(c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)
	// taxpayer := claims["taxpayer"].(string) // Not needed in this query

	// Executes the database query to get all carriers from a given supplier
	var supplierCarriers []users.Employee
	rows, _ := pg.Pool.Query(
		context.Background(),
		`SELECT
			jsonb_array_elements(employees->'carriers')
		FROM
			users
		WHERE
			"user" = $1`,
		user)

	for rows.Next() {
		var carrier users.Employee
		err := rows.Scan(&carrier)
		if err != nil {
			logger.Error.Println(err)
			return c.SendStatus(500)
		}
		supplierCarriers = append(supplierCarriers, carrier)
	}

	// In case no carriers were added yet
	if supplierCarriers == nil {
		supplierCarriers = []users.Employee{}
	}

	// Returns the retrieved carriers
	return c.JSON(supplierCarriers)
}

// NEEDS URGENT REWORK!!!
func SelectSellerSales(c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)
	taxpayer := claims["taxpayer"].(string)

	// Executes the database query to get the seller account
	var account Seller // Seller account data
	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT
			(
				SELECT * FROM
					jsonb_array_elements(employees->'sellers')
				AS
					elem
				WHERE
					(elem->>'taxpayer') = $1
			) AS seller,
			alias
		AS
			seller
		FROM
			users
		WHERE
			"user" = $2`,
		taxpayer, user).Scan(&account.Data, &account.Alias)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	return c.JSON(account)
}

// func SelectSellerSales (c *fiber.Ctx) error {
// 	// Parse attempt being nil causes panic
// 	if c.Locals("authorization") == nil {
// 		c.SendStatus(500)
// 		return
// 	}
// 	// Dumps the content from the JWT token
// 	// token := c.Locals("authorization").(*jwt.Token)
// 	// claims := token.Claims.(jwt.MapClaims)
// 	// user := claims["user"].(string)
// 	// taxpayer := claims["taxpayer"].(string)

// 	return c.SendStatus(200)
// }