package environment

// Postgres database address URL
// postgres://YourUserName:YourPassword@YourHost:5432/YourDatabase

var (
	// Development variables
	// ApiPort       string = ":3001"
	// PgUrl         string = "postgresql://postgres:1234@localhost:5432/postgres?sslmode=disable&pool_max_conns=10"
	// RedisAddr     string = "127.0.0.1:6379"
	// RedisPassword string = ""
	// RedisDb       int    = 0

	// Production variables
	ApiPort       string = ":3000"
	PgUrl         string = "postgresql://postgres:P5t8pbp2QcD9mdGsd2CyPFjwuPjcv474mdpfZLvc@materiappdb.cmupmu1yg0tj.sa-east-1.rds.amazonaws.com:5432/materiapp?sslmode=disable&pool_max_conns=10"
	RedisAddr     string = "materiappsms.t2z7gw.0001.sae1.cache.amazonaws.com:6379"
	RedisPassword string = ""
	RedisDb       int    = 0

	// JSON Web Token 256-bit key
	JwtSigningKey []byte = []byte("v7XtvX5-E@&)DNXQ?WxA-!7t6Q*7*z*A")

	// eNotas token
	EnotasApiToken string = "NzA4MDcwZmUtNzI0Zi00YzBmLTlkMjgtY2JkMmM3ZDkwNjAw"

	// GatewayAPI token
	GatewayApiKey    string = "OCATmJ4tJaFJbL6luMekUNMu"
	GatewayApiSecret string = "EkY!B8@zAnwkycFb5)Pq!(xACbFOX^(3Q@DlZ44X"
	GatewayApiToken  string = "E5ieLSWhSWy-pbjHpFDTLv0oxugtPZnAvXgFdx5IfCGil_PZuBkU6TWsSPYDQlw-"

	// AWS credentials
	AwsAccessKeyId     string = "AKIAJDEWDRQPIAAIVDMQ"
	AwsSecretAccessKey string = "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"
	// AWS S3 settings
	AwsS3BucketName string = "materiapp"
	AwsS3BucketAcl  string = "public-read" // Could be private if you want it to be access by only authorized users
	AwsS3Region     string = "sa-east-1"
	AwsS3AclPolicy  string = "public-read"
)
