package connection

// import "fmt"

// const (
//   host = "localhost"
//   port = 5432
//   user = "postgres"
//   password = "1234"
//   databaseName = "postgres"
// )

// var PgConnectionString string = fmt.Sprintf(
// 	"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", 
// 	host, port, user, password, databaseName)

import (
	"context"
	"os"

	// Local packages
	env "api/helpers/variables/environment"
	"api/helpers/functions/logger"

	// Remote packages
	"github.com/jackc/pgx/v4/pgxpool" // "Query" for returning multiple rows, "QueryRow" for a single row, "Exec" when you do not return any rows (but you will affect existing ones)
)
var	Pool *pgxpool.Pool

func InitializeDatabaseConnection () {
	// poolConfig, err := pgxpool.ParseConfig(os.Getenv("DATABASE_URL"))
	poolConfig, err := pgxpool.ParseConfig(env.PgUrl)
	if err != nil {
		logger.Error.Println(err)
		os.Exit(1)
	}

	Pool, err = pgxpool.ConnectConfig(context.Background(), poolConfig)
	if err != nil {
		logger.Error.Println(err)
		os.Exit(1)
	}

	logger.Info.Println("Connected to Postgres database")
}
