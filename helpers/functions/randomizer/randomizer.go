package randomizer

import (
    "math/rand"

    "unsafe"
)

// All alphanumericals except characters "l", "i", "1", "0", "o"
var alphanumeric = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789"

// Generates a safe random string of fixed size
func GenerateSafeString (size int) string {
    buf := make([]byte, size)
    for i := 0; i < size; i++ {
        buf[i] = alphanumeric[rand.Intn(len(alphanumeric))]
    }
    return string(buf)
}

// var alphabet = []byte("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
// More performant yet more prone to errors due to unsafe
func GenerateRandomString (size int, characters string) string {
    b := make([]byte, size)
    l := len(characters)
    for i := 0; i < size; i++ {
        b[i] = characters[rand.Intn(l)]
    }
    return *(*string)(unsafe.Pointer(&b))
}
