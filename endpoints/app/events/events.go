package events

import (
	// Standard packages
	"context"

	// Remote packages
	"github.com/gofiber/fiber/v2"

	// Local packages
	"api/helpers/functions/logger"
	pg "api/database/postgres/connection"
)

func InsertPrelaunchLead (c *fiber.Ctx) error {
	type Body struct {
		Company string `json:"company"`
		Email   string `json:"email"`
	}
	// Parses the body data
	var body Body  // This struct stores the entire JSON
	if err := c.BodyParser(&body); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	// Executes the database query
	if _, err := pg.Pool.Exec(
		context.Background(),
		"INSERT INTO events_prelaunch (company, email) VALUES ($1, $2)",
		body.Company, body.Email)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	return c.SendStatus(200)
}