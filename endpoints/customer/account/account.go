// Minified table configuration
// user PRIMARY KEY NOT NULL, details JSONB, active BOOLEAN NOT NULL, key TEXT NOT NULL UNIQUE, passphrase TEXT NOT NULL, data JSON

package account

import (
	// Standard packages
	"context"
	// "time"

	// Remote packages
	"github.com/golang-jwt/jwt/v4"
	"github.com/gofiber/fiber/v2"
	// "github.com/alexedwards/argon2id"
	// "github.com/google/uuid"
	
	// Local packages
	// env "api/helpers/variables/environment"
	// "api/helpers/functions/sifter"
	"api/helpers/functions/logger"
	pg "api/database/postgres/connection"
	"api/database/models/users"
	"api/models/response"
)

func SelectCustomerAccount (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)
	// Executes the database query
	var data users.Data
	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT data FROM users WHERE "user" = $1`,
		user).Scan(&data)
	if err != nil {
		logger.Error.Println(err)
		var r response.Default
		r.Error = "ba0"
		return c.JSON(r)
	}
	// Returns the retrieved data
	return c.JSON(data)
}

func UpdateCustomerAccount (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)
	// Parses the body data
	var data users.Data // This struct stores the entire JSON
	if err := c.BodyParser(&data); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	// Executes the database query
	if _, err := pg.Pool.Exec(
		context.Background(),
		`UPDATE users SET data = $1 WHERE "user" = $2`,
		data, user)
		// "UPDATE users SET alias = $1, data = $2 WHERE "user" = $3",
		// data.Email, data, user)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	// Returns a success status
	return c.SendStatus(200)
}