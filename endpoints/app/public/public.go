package public

import (
	// Standard packages
	"context"

	// Remote packages
	"github.com/gofiber/fiber/v2"

	// Local packages
	"api/database/models/items"
	"api/database/models/users"
	pg "api/database/postgres/connection"
	"api/helpers/functions/logger"
)

type StoreDetails struct {
	// Active           bool                     `json:"active"`
	// Alias            string                   `json:"alias"`

	Administrative users.Administrative `json:"administrative"`
	Locality       users.Locality       `json:"locality"`
	Contacts       interface{}          `json:"contacts"`
	Ecommerce      users.Ecommerce      `json:"ecommerce"`
	Environment    users.Environment    `json:"environment"`
	Installments   users.Installments   `json:"installments"`
	Shipping       users.Shipping       `json:"shipping"`
	Categories     []users.Category     `json:"categories"`
	// Employees        users.Employees      `json:"employees"`
	// ServiceWeek      users.BooleanWeek    `json:"serviceWeek"`
	// ServiceSchedule  users.TimetableWeek  `json:"serviceSchedule"`
	// DeliveryWeek     users.BooleanWeek    `json:"deliveryWeek"`
	// DeliverySchedule users.TimetableWeek  `json:"deliverySchedule"`
	// Settings         users.Settings       `json:"settings"`

	Items []items.Item `json:"items"`
}

func Pong (c *fiber.Ctx) error {
	return c.SendString("PONG")
}

func SelectPublicStoreDetails (c *fiber.Ctx) error {
	// Parse attempt being empty causes panic
	alias := c.Params("alias")
	if alias == "" {
		return c.SendStatus(400)
	}
	// Executes the database query to get the supplier number
	var supplier string
	var details StoreDetails
	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT "user", data FROM users WHERE alias = $1`,
		alias).Scan(&supplier, &details)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}


	// Empty arrays initialization
	if details.Categories == nil {
		details.Categories = []users.Category{}
	}

	// Executes the database query to retrieve the items from the supplier
	rows, _ := pg.Pool.Query(
		context.Background(),
		`SELECT
			created_at, updated_at, active, entry, data
		FROM
			items
		WHERE
			supplier = $1
		AND
			active = $2`,
		supplier, true)

	for rows.Next() {
		var item items.Item
		err := rows.Scan(&item.CreatedAt, &item.UpdatedAt, &item.Active, &item.Entry, &item.Data)
		if err != nil {
			logger.Error.Println(err)
			return c.SendStatus(500)
		}
		details.Items = append(details.Items, item)
	}

	// Returns the retrieved data
	return c.JSON(details)
}

func SelectPublicSupplierItem (c *fiber.Ctx) error {
	alias := c.Params("alias")
	entry := c.Params("entry")
	// Executes the database query
	var item items.Item
	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT
			i.active, i.entry, i.data
		FROM
			items i	INNER JOIN suppliers s ON s.user = i.supplier
		WHERE
			s.alias = $1 AND i.entry = $2`,
		alias, entry).Scan(&item.Active, &item.Entry, &item.Data)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(500)
	}
	// Returns the retrieved data
	return c.JSON(item)
}

func SelectPublicSupplierItemsFromDescription (c *fiber.Ctx) error { // Postgres Full Text Search
	description := c.Params("description")

	list := []items.Item{}
	// Executes the database query to retrieve the items with the given description from the database
	rows, _ := pg.Pool.Query(
		context.Background(),
		`SELECT
			active, entry, data
		FROM
			items
		WHERE
			to_tsvector('portuguese', data->'control'->>'description') @@ plainto_tsquery('portuguese', unaccent($1))
		AND
			active = $2`,
		description, true)

	for rows.Next() {
		var item items.Item
		err := rows.Scan(
			// &i.CreatedAt,
			// &i.UpdatedAt,
			&item.Active,
			&item.Entry,
			&item.Data)
		if err != nil {
			logger.Error.Println(err)
			return c.SendStatus(500)
		}
		list = append(list, item)
	}

	// Returns the retrieved data
	return c.JSON(list)
}