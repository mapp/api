// Retrieves the latest added supplier (serial) from the table suppliers
// SELECT currval(pg_get_serial_sequence("suppliers", 'supplier"));

package authorization

import (
	// Standard packages
	"context"
	"time"

	// Remote packages
	"github.com/golang-jwt/jwt/v4"
	"github.com/gofiber/fiber/v2"
	"github.com/alexedwards/argon2id"

	// Local packages
	env "api/helpers/variables/environment"
	"api/helpers/functions/logger"
	pg "api/database/postgres/connection"
	"api/database/models/users"
)

// func prettyPrintJson(i interface{}) string {
//     s, _ := json.MarshalIndent(i, "", "\t")
//     return string(s)
// }

type Payload struct {
	Alias     string                  `json:"alias"`
	Data      users.Data              `json:"data"`
	Settings  users.Settings          `json:"settings"`
	Employees users.EmployeesRegister `json:"employees"`
	// Privileges Privileges `json:"privileges"` // Subscription plan: 0 -> free, 1 -> paid, 2 -> 
}

func AuthenticateSupplier (c *fiber.Ctx) error {
	var access users.AuthorizationRequest // This struct stores the entire JSON
	if err := c.BodyParser(&access); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// timestamp := uint64(time.Now().Unix()) // Updates the last session property at the details column

	var (
		user    string
		hash    string
		payload Payload
	)
	// err := pg.Pool.QueryRow(
	// 	context.Background(),
	// 	"UPDATE users SET details = jsonb_set(details, '{lastSession}', to_jsonb($1::int)) WHERE alias = $2 RETURNING user, passphrase, alias, data, settings, employees, privileges",
	// 	timestamp, access.Taxpayer).Scan(&user, &hash, &payload.Alias, &payload.Data, &payload.Settings, &payload.Employees, &payload.Privileges);
	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT "user", passphrase, alias, data, settings, employees FROM users WHERE taxpayer = $1`,
		&access.Taxpayer).Scan(&user, &hash, &payload.Alias, &payload.Data, &payload.Settings, &payload.Employees);
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(401)
	}

	// Empty arrays initialization
	// if payload.Data.Categories == nil {
	// 	payload.Data.Categories = []users.Category{}
	// }

	match, err := argon2id.ComparePasswordAndHash(access.Passphrase, hash)
	if err != nil && hash != "" { // HASH == "" IS A TEMPORARY MEASURE!
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	if match || hash == "" { // If hash is empty it means the supplier is new and should set a new passphrase (At the "Account" page)
		// Create token
		token := jwt.New(jwt.SigningMethodHS256)
		// Set claims
		claims := token.Claims.(jwt.MapClaims)
		claims["user"] = user
		claims["exp"] = time.Now().Add(time.Hour * 72).Unix()
		// Generate encoded token and send it as response
		encodedToken, err := token.SignedString(env.JwtSigningKey)
		if err != nil {
			logger.Error.Println(err)
			return c.SendStatus(500)
		}

		var sd users.AuthorizationResponse
		sd.Token   = &encodedToken
		sd.Payload = &payload
		return c.JSON(sd)
	} else {
		return c.SendStatus(404)
		// var r response.Default
		// r.Error = "ba1"
		// c.JSON(r)
	}
}