package constants

var (
	// Only allow files under 1Mb size
	MaximumFileSize = int64(1024000)
	// Max quantity of files allowed per upload session
	UploadFilesLimit = 8
)