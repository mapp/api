package items

import (
	// Standard packages
	"context"
	"encoding/json"
	"strconv"
	"time"
	"fmt"

	// Remote packages
	"github.com/golang-jwt/jwt/v4"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"

	// Local packages
	"api/services/aws/s3"
	"api/helpers/variables/constants"
	"api/helpers/functions/logger"
	"api/helpers/functions/concat"
	pg "api/database/postgres/connection"
	"api/database/models/items"
	"api/models/response"
 )

func SelectSupplierItemsTotal (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	// Executes the database query
	var itemsTotal uint64
	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT count(*) FROM items WHERE supplier = $1`,
		user).Scan(&itemsTotal)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// Returns the retrieved data
	return c.JSON(fiber.Map{
		"itemsTotal": itemsTotal,
	})
}

// This endpoint receives a multipart/form-data with the keys,
// "entry" as an integer,
// "mediaBuffer" as an image file,
// "data" as a stringified JSON
func InsertSupplierItem (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	// Retrieves the first value from the given key within the multipart/form-data
	// Gets the new item's entry number
	entry, err := strconv.ParseUint(c.FormValue("entry"), 10, 64)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	// Casts and unmarshals the data string retrieved from the multipart/form-data body
	dataBytes := []byte(c.FormValue("data"))
	var data items.Data
    if err := json.Unmarshal(dataBytes, &data); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(500)
    }
	// Parse the multipart/form-dta
	form, err := c.MultipartForm() // => *multipart.Form
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(500)
	}
	// Get all files from "images" key
	files := form.File["images"]
	// => []*multipart.FileHeader

	session := s3.NewSession()

	remoteImages := []string{}
	// for _, file := range files
	totalFiles := len(files)
	// Loop through files
	for i := 0; i < totalFiles; {
		file := files[i]
		// Security measure to not let the application hang up while processing unexpected sizes
		if file.Size <= constants.MaximumFileSize {

			location := concat.Join("items/", c.FormValue("alias"), "/", c.FormValue("entry"), "/")
			filename, err := s3.UploadFile(session, file, location); if err != nil {
				logger.Error.Println(err)
			}
			remoteImages = append(remoteImages, filename)

			i++
		} else {
			logger.Error.Println(fmt.Errorf("file is too large: %v", file.Size))
		}

		// The process stops if the upload count reaches the system limit before all available files were uploaded
		if (i == constants.UploadFilesLimit) {
			i = totalFiles
		}
	}
    // Uploaded image(s) URLs are stored within the struct member
    var media items.Media
    media.Images = remoteImages
    // Updates the struct member
    data.Media = media

    // Sets the Unix timestamps for the new item
	createdAt := uint64(time.Now().Unix())
	updatedAt := createdAt

	item := uuid.New()
	// Executes the database query
	if _, err := pg.Pool.Exec(
		context.Background(),
		`INSERT INTO items (item, active, supplier, entry, data, created_at, updated_at) VALUES ($1, $2, $3, $4, $5, $6, $7)`,
		item, true, user, entry, data, createdAt, updatedAt)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// To get the "media" key from the response body (data) it needs to be encoded
	// This reponse is only needed if any images were sent in the request
	var r response.Item
	if len(files) > 0 {
		r.Media = media
		return c.JSON(r)
	}

	return c.SendStatus(200)
}

func SelectSupplierItems (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	// Executes the database query
	list := []items.Item{}
	rows, _ := pg.Pool.Query(
		context.Background(),
		`SELECT created_at, updated_at, active, entry, data FROM items WHERE supplier = $1`,
		user)

	for rows.Next() {
		var item items.Item
		err := rows.Scan(&item.CreatedAt, &item.UpdatedAt, &item.Active, &item.Entry, &item.Data)
		if err != nil {
			logger.Error.Println(err)
			return c.SendStatus(500)
		}
		
		list = append(list, item)
	}

	// Returns the retrieved data
	return c.JSON(list)
}

func SelectSupplierItem (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	supplier := claims["user"].(string)

	entry := c.Params("entry")
	// Executes the database query
	var item items.Item
	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT
			created_at, updated_at, active, entry, data
		FROM
			items
		WHERE
			supplier = $1
		AND
			entry = $2`,
		supplier, entry).Scan(&item.CreatedAt, &item.UpdatedAt, &item.Active, &item.Entry, &item.Data)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(500)
	}
	// Returns the retrieved data
	return c.JSON(item)
}

func UpdateSupplierItemData (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	// Casts and unmarshals the data string retrieved from the multipart/form-data body
	dataBytes := []byte(c.FormValue("data"))
	var data items.Data
    if err := json.Unmarshal(dataBytes, &data); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(500)
    }

	// Retrieves the first value from the given key within the multipart/form-data
	entry, err := strconv.ParseInt(c.FormValue("entry"), 10, 64)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// Parse the multipart/form-data
	form, err := c.MultipartForm() // => *multipart.Form
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(500)
	}

	// Get all files from "images" key
	files := form.File["images"]
	// => []*multipart.FileHeader

	session := s3.NewSession()

	remoteImages := []string{}
	// for _, file := range files
	totalFiles := len(files)
	// Loop through files
	for i := 0; i < totalFiles; {
		file := files[i]
		// Security measure to not let the application hang up while processing unexpected sizes
		if file.Size <= constants.MaximumFileSize {

			location := concat.Join("items/", c.FormValue("alias"), "/", c.FormValue("entry"), "/")
			filename, err := s3.UploadFile(session, file, location); if err != nil {
				logger.Error.Println(err)
			}
			remoteImages = append(remoteImages, filename)

			i++
		} else {
			logger.Error.Println(fmt.Errorf("file is too large: %v", file.Size))
		}

		// The process stops if the upload count reaches the system limit before all available files were uploaded
		if (i == constants.UploadFilesLimit) {
			i = totalFiles
		}
	}
    // Uploaded image(s) URLs are stored within the struct member
    var media items.Media
    // Concatenates the existing images with any new ones
    media.Images = append(data.Media.Images, remoteImages...)
    // Updates the struct member
    data.Media = media

    // Sets the Unix timestamps for the new item
	// data.Control.UpdatedAt = data.Control.CreatedAt

	timestamp := strconv.FormatUint(uint64(time.Now().Unix()), 10)
	// Executes the database query
	if _, err := pg.Pool.Exec(
		context.Background(),
		`UPDATE
			items
		SET
			updated_at = $1, data = $2
		WHERE
			"supplier" = $3
		AND
			entry = $4`,
		timestamp, data, user, entry);
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// To get the "media" key from the response body (data) it needs to be encoded
	// This reponse is only needed if any images were sent in the request
	var r response.Item
	if len(files) > 0 {
		r.Media = media
		return c.JSON(r)
	}

	return c.SendString("{}")
}

func UpdateSupplierItemActivity (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	// Retrieves the first value from the given key within the multipart/form-data
	entry, err := strconv.ParseInt(c.FormValue("entry"), 10, 64)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	activity, err := strconv.ParseBool(c.FormValue("active"))
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	timestamp := strconv.FormatUint(uint64(time.Now().Unix()), 10)
	// Executes the database query
	if _, err := pg.Pool.Exec(
		context.Background(),
		`UPDATE
			items
		SET
			updated_at = $1, active = $2
		WHERE
			supplier = $3
		AND
			entry = $4`,
		timestamp, activity, user, entry);
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	return c.SendStatus(200)
}


func UpdateSupplierItemDataGroupsCategory (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	type Body struct {
		NewTag string `json:"newTag"`
		OldTag string `json:"oldTag"`
	}
	// Parses the body data
	var body Body  // This struct stores the entire JSON
	if err := c.BodyParser(&body); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	timestamp := strconv.FormatUint(uint64(time.Now().Unix()), 10)
	// Executes the database query
	if _, err := pg.Pool.Exec(
		context.Background(),
		// "UPDATE items SET data->'groups'->'category' = data->'groups'->>'category' || $1 WHERE data->'groups'->>'category' = $2 AND "user" = $3",
		// "UPDATE itens SET data = data || {\"groups\": to_jsonb($2::text)} WHERE data->'groups'->>'category' = $2 AND "user" = $3",
		`UPDATE
			items
		SET
			updated_at = $1, data = jsonb_set(data, '{groups,category}', to_jsonb($2::text))
		WHERE
			data->'groups'->>'category' = $3
		AND
			supplier = $4`,
		timestamp, body.NewTag, body.OldTag, user);
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	return c.SendStatus(200)
}



// May require further refinement
func UpdateSupplierItemDataControlQuantity (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	type Body struct {
		Entry    uint64  `json:"entry"`
		Quantity float64 `json:"quantity"`
	}
	// Parses the body data
	var body Body  // This struct stores the entire JSON
	if err := c.BodyParser(&body); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// Retrieves the item data to update the quantity (more precisely)
	var active bool
	var data items.Data
	if err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT
			active, data
		FROM
			items
		WHERE
			"user" = $1
		AND
			entry = $2`,
		user, body.Entry).Scan(&active, &data)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(500)
	}

	// Automatically turns the item active again if the option is checked
	if active == false && data.Control.AutoDeactivatable == true {
		active = true
	}
	// Increments the current quantity of the item
	data.Control.Quantity = data.Control.Quantity + body.Quantity
	// TODO: Take in consideration discount

	timestamp := strconv.FormatUint(uint64(time.Now().Unix()), 10)
	// Updates the quantity in the data and maybe the active status/the discount quota
	if _, err := pg.Pool.Exec(
		context.Background(),
		`UPDATE
			items
		SET
			updated_at = $1, active = $2, data = $3
		WHERE
			"user" = $4
		AND
			entry = $5`,
		timestamp, active, data, user, body.Entry)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	return c.SendStatus(200)
}

func DeleteSupplierItemDataMediaImage (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	var images []string
	if err := c.BodyParser(&images); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	entry := c.Params("entry")
	// element := c.Params("element")
	timestamp := strconv.FormatUint(uint64(time.Now().Unix()), 10)
	// Executes the database query
	if _, err := pg.Pool.Exec(
		context.Background(),
		`UPDATE
			items
		SET
			updated_at = $1, data->'media'->'images' = data->'media'->>'images' || $2
		WHERE
			"user" = $3
		AND
			entry = $4`,
		timestamp, images, user, entry);
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	return c.SendStatus(200)
}

	// // Parse the multipart form
	// if form, err := c.MultipartForm(); // => *multipart.Form
	// err != nil {
	// 	return c.SendStatus(500)
	// 	return
	// }
	// // Get all files from "documents" key
	// files := form.File["documents"]
	// // => []*multipart.FileHeader

	// var resources string = []string{}
	// // Loop through files
	// for _, file := range files {
	// 	// Replace this line for actual URIs from DO Object storage reponse
	// 	resources = append(resources, file.Filename)
	// }

	// // Loop through files:
	// for _, file := range files {
	// 	// Add the feature of uploading each file to Digital Ocean and retrieving the URLs
	// 	fmt.Println(file.Filename, file.Size, file.Header["Content-Type"][0])
	// 	// => "tutorial.pdf" 360641 "application/pdf"

	// 	// Save the files to disk:
	// 	c.SaveFile(file, fmt.Sprintf("./%s", file.Filename))
	// }