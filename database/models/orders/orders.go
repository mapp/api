// Minified table configuration
// record TEXT NOT NULL, created_at BIGINT NOT NULL, updated_at BIGINT NOT NULL, customer UUID, supplier UUID NOT NULL, data JSONB NOT NULL

package orders

import (
	"api/database/models/items"
	"api/database/models/users"
)

type Content struct {
	Item     items.Item `json:"item"`
	Amount   float64    `json:"amount"`
	Returned uint64     `json:"returned"`
}

// type Window struct {
// 	Opening users.Timeframe `json:"opening"`
// 	Closing users.Timeframe `json:"closing"`
// }
type Carrier struct {
	Taxpayer    string `json:"taxpayer"`
	Internal    bool   `json:"internal"`
	Accountable bool   `json:"accountable"`
}
type Coordinates struct {
	Latitude  float32 `json:"latitude"`
	Longitude float32 `json:"longitude"`
}
type Geoframe struct {
	Coordinates Coordinates `json:"coordinates"`
	Timestamp   uint64      `json:"timestamp"`
}
type Delivery struct {
	AtSupplier bool           `json:"atSupplier"`
	Address    users.Address  `json:"address"`
	// Status    uint8              `json:"status"` // Inactive, active, postponed
	Shipping   users.Shipping `json:"shipping"`
	// Window    users.Timeframe `json:"window"`
	Carriers   []Carrier      `json:"carriers"`
	// Geodata   []Geoframe          `json:"geodata"`   // An array with only two positions, the first value is the point where the carrier left and the second is the point where the carrier finished the shipping. In the future, it will store the coordinates for the whole trajectory
	// Odograph  float32             `json:"odograph"`  // It works as a shortcut to store the distance travelled by the carrier (no need to calculate it from the array of coordinates and therefore, saving processing power)
	Signature  string         `json:"signature"`
}

// type Invoice struct {
// 	Issuer    string      `json:"issuer"`    // eNotas or the company responsible for issuing it
// 	Revisions interface{} `json:"revisions"` // An array containing all invoices (purchasing, returning, importing, exporting, etc) 
// }
// type Installments struct {
// 	// Active     bool   `json:"active"`
// 	Times      uint8  `json:"times"`
// 	BeginsAt   uint64 `json:"beginsAt"`
// 	Signature  string `json:"signature"`
// }
type Payment struct {
	Value          float32            `json:"value"`        // Value attribute only in case for multiple payment methods at checkout
	Method         uint8              `json:"method"`
	ExpirationDate uint64        	  `json:"expirationDate"`						// Lookup table storing how the payment was done (Cash, credit card, etc)
	InAdvance      bool               `json:"inAdvance"`
	Installments users.Installments `json:"installments"` 
	// Invoice      Invoice                `json:"invoice"`      // eNotas field
	// // Voucher      string                 `json:"voucher"`      // The discount code provided by the supplier
	// // Discount     float32                `json:"discount"`     // The discount number itself (also used for arbitrary discounts without coupons)
	// // Total        float32                `json:"total"`        // Total to be paid with all products, interests, shipping and discount
	// Details      interface{}            `json:"details"`      // Stripe/Paypal field
	Details      interface{}        `json:"details"`      // The content here depends on the method chosen
}
type Seller struct {
	Taxpayer string `json:"taxpayer"`
	Internal bool   `json:"internal"`
}
type Buyer struct { // Optional information from who is buying
	Taxpayer string `json:"taxpayer"`
	Note     string `json:"note"`
}

type Data struct {
	Alias    string           `json:"alias"`
	Buyer    Buyer            `json:"buyer"`
	// Alias  string     `json:"alias"`
	Content  []Content        `json:"content"`
	Delivery Delivery         `json:"delivery"`
	Status   uint8            `json:"status"`
	Payment  []Payment        `json:"payment"` // Array only in case for multiple payment methods at checkout
	Seller   Seller           `json:"seller"`

	// Invoice      Invoice                `json:"invoice"`      // eNotas field
	// Voucher      string                 `json:"voucher"`      // The discount code provided by the supplier
	// Discount     float32                `json:"discount"`     // The discount number itself (also used for arbitrary discounts without coupons)
	// Total        float32                `json:"total"`        // Total to be paid with all products, interests, shipping and discount
	// Details      interface{}            `json:"details"`      // Stripe/Paypal field
}

type Order struct { // Postgre table schema
	Record    string      `json:"record"`
	CreatedAt uint64      `json:"createdAt"`
	UpdatedAt uint64      `json:"updatedAt"`
	Customer  interface{} `json:"customer"` // Can be string (uuid) or nil
	Supplier  string      `json:"supplier"` // Record, Customer and Supplier are confidential and should not be exposed outside the server
	Data      Data        `json:"data"`
}

// internal to plataform
type SupplierOrder struct {
	CustomerData users.Data `json:"customerData"`
	Record       string         `json:"record"`
	CreatedAt    uint64         `json:"createdAt"`
	UpdatedAt    uint64         `json:"updatedAt"`
	// Customer  string         `json:"customer"`
	// Supplier  string         `json:"supplier"` // Record, Customer and Supplier are confidential and should not be exposed outside the server
	OrderData    Data           `json:"orderData"`
}
// supplier order without sensitive information
type GeneralUsersOrder struct {
	Record    string `json:"record"`
	CreatedAt uint64 `json:"createdAt"`
	UpdatedAt uint64 `json:"updatedAt"`
	Data      Data   `json:"data"`
}