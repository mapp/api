package connection

import (
	"os"
    "context"

	// Local packages
	env "api/helpers/variables/environment"
	"api/helpers/functions/logger"

    "github.com/go-redis/redis/v8"
)
var ctx = context.Background()

func NewClient () *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     env.RedisAddr,
		Password: env.RedisPassword,
		DB:       env.RedisDb,
	})

	return client
}

func PingDatabase () {
	client := NewClient()

	ctx := context.Background()

	_, err := client.Ping(ctx).Result()
	if err != nil {
		logger.Error.Println(err)
		os.Exit(1)
	}

	logger.Info.Println("Ping to Redis database successful")
}