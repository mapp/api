package invoices

import (
	// Standard packages
	"context"
	"io/ioutil"
	"time"

	// "strings"
	// "time"

	// "fmt"
	// "io/ioutil"

	// Remote packages
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"

	// "github.com/google/uuid"

	// Local packages
	// "api/database/models/invoices"
	"api/database/models/users"
	pg "api/database/postgres/connection"

	// "api/helpers/functions/concat"
	"api/helpers/functions/logger"
	// env "api/helpers/variables/environment"
	// "api/models/response"
	"api/services/enotas"
)

// func sortBrazilianInvoiceType (model int) string {
// 	if (model == 1) {
// 		return "nf-e"
// 	} else if (model == 2) {
// 		return "ct-e"
// 	} else if (model == 3) {
// 		return "nfs-e"
// 	} else if (model == 4) {
// 		return "nfc-e"
// 	}
// }

type InvoiceParameters struct {
	BusinessId string      `json:"businessId"`
	Data       interface{} `json:"data"`
	Model      uint8       `json:"model"`
	Order      string      `json:"order"` // UUID
}

// func InsertOrderInvoice (c *fiber.Ctx) error {
// 	// Parse attempt being nil causes panic
// 	if c.Locals("authorization") == nil {
// 		return c.SendStatus(500)
// 	}
// 	// Dumps the content from the JWT token
// 	token := c.Locals("authorization").(*jwt.Token)
// 	claims := token.Claims.(jwt.MapClaims)
// 	user := claims["user"].(string)

// 	// Gets the supplier ID within eNotas service
// 	// businessId := c.Params("businessId")

// 	var invoice InvoiceParameters
// 	if err := c.BodyParser(&InvoiceParameters); err != nil {
// 		logger.Error.Println(err)
// 		return c.SendStatus(400)
// 	}

// 	payload := strings.NewReader(invoice.data)
// 	url := concat.Join("https://api.enotasgw.com.br/v2/empresas/", invoice.businessId, "/", sortBrazilianInvoiceType(invoice.model))
// 	req, _ := http.NewRequest("POST", url, payload)
// 	req.Header.Add("Accept", "application/json")
// 	req.Header.Add("Content-Type", "application/json")
// 	req.Header.Add("Authorization", concat.Join("Basic ", env.enotasApiToken))

// 	res, _ := http.DefaultClient.Do(req)
// 	defer res.Body.Close()
// 	body, _ := ioutil.ReadAll(res.Body)

// 	fmt.Println(res)
// 	fmt.Println(string(body))
// 	// HOW TO PROCESS ALL THAT STUFF, NEED TO DO IT
// 	details := "" // This is the response from eNotas, whatever it is

// 	record := uuid.New()
// 	createdAt := uint64(time.Now().Unix())
// 	updatedAt := createdAt

// 	if _, err := pg.Pool.Exec(
// 		context.Background(),
// 		"INSERT INTO invoices (record, model, order, status, details, created_at, updated_at) VALUES ($1, $2, $3, $4, $5, $6, $7)",
// 		record, invoice.model, invoice.order, 0, details, createdAt, updatedAt)
// 	err != nil {
// 		logger.Error.Println(err)
// 		return c.SendStatus(400)
// 	}
// 	// return c.SendStatus(200)
// 	c.SendString(body)
// }

func GenerateBusinessId (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	var payload interface{}
	if err := c.BodyParser(&payload); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// logger.Error.Println(payload)

	businessId, err := enotas.EnotasNewBusiness(payload); if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	logger.Error.Println(businessId)

	var data users.Data
	// Executes the database query
	if err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT data FROM users WHERE "user" = $1`,
		user).Scan(&data)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	data.Miscellaneous.Enotas.BusinessId = businessId

	logger.Error.Println(data)
	logger.Error.Println(user)


	if _, err := pg.Pool.Exec(
		context.Background(),
		`UPDATE users SET data = $1 WHERE "user" = $2`,
		data, user)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// Returns a success status
	return c.SendStatus(200)
}


func NfceEmission (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	payload := struct{
		Data interface{} `json:"data"`
		Order []string `json:"order"`
	}{}
	if err := c.BodyParser(&payload); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// logger.Error.Println(payload)

	var businessId string
	// Executes the database query
	if err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT data -> 'miscellaneous' -> 'enotas' ->> 'businessId' FROM users WHERE "user" = $1`,
		user).Scan(&businessId)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	emissionResponse, err := enotas.NfceEmission(payload.Data, businessId); if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	record := uuid.New()
	createdAt := uint64(time.Now().Unix())
	updatedAt := createdAt

	if _, err := pg.Pool.Exec(
		context.Background(),
		`INSERT INTO
			 users (record, status, created_at, updated_at, model, status, detail, order)
		VALUES
			($1, $2, $3, $4, $5, $6, $7)`,
		record, createdAt, updatedAt, nil, emissionResponse.Status, emissionResponse,  payload.Order)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// Returns a success status
	return c.SendStatus(200)

}

func LinkCertificate( c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	data, err := c.FormFile("arquivo")
	 if err != nil {
	 	return err
	}

	password := c.FormValue("senha")

	var businessId string
	// Executes the database query
	if err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT data -> 'miscellaneous' -> 'enotas' ->> 'businessId' FROM users WHERE "user" = $1`,
		user).Scan(&businessId)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	resLinkCertificate, err := enotas.EnotasLinkCertificate(businessId, *data, password)
	if err != nil{
		logger.Error.Println(err)
	}
	if resLinkCertificate.StatusCode != 200{
		r, err :=ioutil.ReadAll(resLinkCertificate.Body)
		if err != nil{
			logger.Error.Println(err)
		}
		return c.Status(resLinkCertificate.StatusCode).SendString(string(r))
	}
	defer resLinkCertificate.Body.Close()
	r, err :=ioutil.ReadAll(resLinkCertificate.Body)
	if err != nil{
		logger.Error.Println(err)
	}

	return c.Status(200).SendString(string(r))
}

func LinkLogo(c *fiber.Ctx) error{
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	data, err := c.FormFile("logotipo")
	if err != nil {
		return err
	}

	var businessId string
	//Executes the database query
	if err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT data -> 'miscellaneous' -> 'enotas' ->> 'businessId' FROM users WHERE "user" = $1`,
		user).Scan(&businessId)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	resLinkLogo, err := enotas.EnotasLinkLogo(businessId, *data)
	if err != nil {
		logger.Error.Println(err)
	}
	if resLinkLogo.StatusCode != 200{
		r, err :=ioutil.ReadAll(resLinkLogo.Body)
		if err != nil{
			logger.Error.Println(err)
		}
		return c.Status(resLinkLogo.StatusCode).SendString(string(r))
	}

	r, err :=ioutil.ReadAll(resLinkLogo.Body)
	if err != nil{
		logger.Error.Println(err)
	}
	return c.Status(resLinkLogo.StatusCode).SendString(string(r))
}

func SearchNfe ( c *fiber.Ctx) error{
	// // Parse attempt being nil causes panic
	// if c.Locals("authorization") == nil {
	// 	return c.SendStatus(500)
	// }
	// // Dumps the content from the JWT token
	// token := c.Locals("authorization").(*jwt.Token)
	// claims := token.Claims.(jwt.MapClaims)
	// user := claims["user"].(string)

	var businessId  = "2D16D146-971A-4CA2-AEDF-6A514B4A0700"
	// Executes the database query
	// if err := pg.Pool.QueryRow(
	// 	context.Background(),
	// 	`SELECT data -> 'miscellaneous' -> 'enotas' ->> 'businessId' FROM users WHERE "user" = $1`,
	// 	user).Scan(&businessId)
	// err != nil {
	// 	logger.Error.Println(err)
	// 	return c.SendStatus(400)
	// }

	nfeId := c.Params("nfeid")

	resSearchNfe, err := enotas.EnotasSearchNfe(businessId, nfeId)
	if err != nil {
		logger.Error.Println(err)
	}
	if resSearchNfe.StatusCode != 200{
		r, err :=ioutil.ReadAll(resSearchNfe.Body)
		if err != nil{
			logger.Error.Println(err)
		}
		return c.Status(resSearchNfe.StatusCode).SendString(string(r))
	}

	r, err :=ioutil.ReadAll(resSearchNfe.Body)
	if err != nil{
		logger.Error.Println(err)
	}
	return c.Status(resSearchNfe.StatusCode).SendString(string(r))
}