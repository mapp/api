package orders

import (
	// Standard packages
	"context"
	"time"
	"encoding/json"
	"strings"
	"fmt"
	"net/http"
	"io/ioutil"
	"strconv"

	// Remote packages
	"github.com/golang-jwt/jwt/v4"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	
	// Local packages
	"api/helpers/functions/logger"
	env "api/helpers/variables/environment"
	pg "api/database/postgres/connection"
	"api/database/models/orders"
	"api/services/aws/s3"
	"api/helpers/functions/concat"
)

// Retrieves all orders from a given supplier uuid
func SelectSupplierOrders (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)
	// Executes the database query

	list := []orders.SupplierOrder{}
	rows, _ := pg.Pool.Query(
		context.Background(),
		`SELECT
			users.data,
			orders.record,
			orders.created_at,
			orders.updated_at,
			orders.data
		FROM
			users
		INNER JOIN
			orders
		ON
			users.user = orders.supplier
		WHERE
			"user" = $1`,
		user)

	for rows.Next() {
		var order orders.SupplierOrder
		err := rows.Scan(&order.CustomerData, &order.Record, &order.CreatedAt, &order.UpdatedAt, &order.OrderData)
		if err != nil {
			logger.Error.Println(err)
			return c.SendStatus(500)
		}
		// Empty array initializations
		if order.OrderData.Delivery.Carriers == nil {
			order.OrderData.Delivery.Carriers = []orders.Carrier{}
		}
		// if order.OrderData.Delivery.Geodata == nil {
		// 	order.OrderData.Delivery.Geodata = []orders.Geoframe{}
		// }
		list = append(list, order)
	}

	// Returns the retrieved orders
	return c.JSON(list)
}

func SelectSupplierOrder (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	supplier := claims["user"].(string)

	record := c.Params("record")
	// Executes the database query
	var order orders.SupplierOrder
	// Query used to retrieve the data from a given order along with the customer account data
	// that created it. When the customer is not registered yet it the column field is null
	// which makes impossible to retrieve the account data for obvious reasons, therefore
	// the field must be retrieved as null.
	//
	// SELECT * FROM table WHERE criteria2 = criteria2
	// and not exists (
	//     SELECT * FROM table WHERE criteria = criteria
	// )
	// union all
	// SELECT * FROM table WHERE criteria = criteria;
	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT
			NULL AS customerData,
			record,
			created_at,
			updated_at,
			data
		FROM
			orders
		WHERE
			supplier = $1
		AND
			record = $2
		AND NOT EXISTS (
			SELECT
				users.data AS customerData,
				orders.record,
				orders.created_at,
				orders.updated_at,
				orders.data
			FROM
				users
			INNER JOIN
				orders
			ON
				users.user = orders.customer
			WHERE
				supplier = $1
			AND
				record = $2
		)
		UNION ALL SELECT
			users.data AS customerData,
			orders.record,
			orders.created_at,
			orders.updated_at,
			orders.data
		FROM
			users
		INNER JOIN
			orders
		ON
			users.user = orders.customer
		WHERE
			supplier = $1
		AND
			record = $2`,
		supplier, record).Scan(&order.CustomerData, &order.Record, &order.CreatedAt, &order.UpdatedAt, &order.OrderData)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(500)
	}
	// Empty array initializations
	if order.OrderData.Delivery.Carriers == nil {
		order.OrderData.Delivery.Carriers = []orders.Carrier{}
	}
	// if order.OrderData.Delivery.Geodata == nil {
	// 	order.OrderData.Delivery.Geodata = []orders.Geoframe{}
	// }
	// Returns the retrieved data
	return c.JSON(order)
}

// Updates the supplier order (status)
func UpdateSupplierOrderData (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	supplier := claims["user"].(string)
	// Parses the body data
	var order orders.Order // This struct stores the entire JSON
	if err := c.BodyParser(&order); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	order.Supplier = supplier
	// Updates the Unix timestamp
	order.UpdatedAt = uint64(time.Now().Unix())

	// Executes the database query
	if _, err := pg.Pool.Exec(
		context.Background(),
		`UPDATE
			orders
		SET
			updated_at = $1,
			data = $2
		WHERE
			supplier = $3
		AND
			record = $4`,
		order.UpdatedAt, order.Data, order.Supplier, order.Record)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

		return c.JSON(fiber.Map{
		"updatedAt": order.UpdatedAt,
	})
}


// This endpoint receives a multipart/form-data with the keys,
// "entry" as an integer,
// "mediaBuffer" as an image file,
// "data" as a stringified JSON 
func InsertSupplierOrderSignature (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	supplier := claims["user"].(string)

	// Casts and unmarshals the data string retrieved from the multipart/form-data body
	dataBytes := []byte(c.FormValue("data"))
	var order orders.Order
    if err := json.Unmarshal(dataBytes, &order); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(500)
    }
	// Parse the file
	file, err := c.FormFile("signature")
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(500)
	}

	session := s3.NewSession()

	location := concat.Join("signatures/", c.FormValue("alias"), "/orders/", order.Record, "/")
	filename, err := s3.UploadFile(session, file, location); if err != nil {
		logger.Error.Println(err)
	}

    // Uploaded image(s) URLs are stored within the struct member
	order.Supplier = supplier
	order.Data.Delivery.Signature = filename
    // Updates the Unix timestamp
	order.UpdatedAt = uint64(time.Now().Unix())

	// Executes the database query
	if _, err := pg.Pool.Exec(
		context.Background(),
		`UPDATE
			orders
		SET
			updated_at = $1,
			data = $2
		WHERE
			supplier = $3
		AND
			record = $4`,
		order.UpdatedAt, order.Data, order.Supplier, order.Record)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	return c.JSON(fiber.Map{
		"signature": filename,
	})
}





// // Updates the supplier order (status)
// func UpdateSupplierOrderData (c *fiber.Ctx) error {
// 	// Parse attempt being nil causes panic
// 	if c.Locals("authorization") == nil {
// 		return c.SendStatus(500)
// 		return
// 	}
// 	// Dumps the content from the JWT token
// 	token := c.Locals("authorization").(*jwt.Token)
// 	claims := token.Claims.(jwt.MapClaims)
// 	user := claims["user"].(string)
// 	// Parses the body data
// 	var order orders.Order // This struct stores the entire JSON
// 	if err := c.BodyParser(&order); err != nil {
// 		logger.Error.Println(err)
// 		return c.SendStatus(400)
// 		return
// 	}

// 	order.Supplier = supplier
//     // Updates the Unix timestamp
// 	order.UpdatedAt = uint64(time.Now().Unix())
// 	// Executes the database query
// 	if _, err := pg.Pool.Exec(
// 		context.Background(),
// 		"UPDATE orders SET updated_at = $1, data = data || {\"status\": to_jsonb($2::int), \"carrier\": to_jsonb($3::text)} WHERE "user" = $4 AND record = $5",
// 		order.UpdatedAt, order.Data.Status, order.Data.Carrier, order.Supplier, order.Record)
// 	err != nil {
// 		logger.Error.Println(err)
// 		return c.SendStatus(400)
// 		return
// 	}

// 	return c.JSON(fiber.Map{
//         "updatedAt": order.UpdatedAt,
//     })
// }



func sortBrazilianInvoiceType (model uint64) string {
	if (model == 1) {
		return "nf-e"
	} else if (model == 2) {
		return "ct-e"
	} else if (model == 3) {
		return "nfs-e"
	}
	return "nfc-e"
}

func InsertSupplierOrderInvoice (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)

	var invoiceData interface{}
	if err := c.BodyParser(&invoiceData); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	orderRecord := c.Params("record")
	invoiceModel, err := strconv.ParseUint(c.Params("model"), 10, 64)
	var businessId string
	err = pg.Pool.QueryRow(
		context.Background(),
		`SELECT
			data->administrative->miscellaneous->enotas->businessId
		FROM
			users
		WHERE
			"user" = $1`,
		user).Scan(&businessId)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	payload := strings.NewReader(invoiceData.(string))
	url := concat.Join("https://api.enotasgw.com.br/v2/empresas/", businessId, "/", sortBrazilianInvoiceType(invoiceModel))
	req, _ := http.NewRequest("POST", url, payload)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", concat.Join("Basic ", env.EnotasApiToken))

	res, _ := http.DefaultClient.Do(req)
	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))
	// HOW TO PROCESS ALL THAT STUFF, NEED TO DO IT
	details := "" // This is the response from eNotas, whatever it is

	invoiceRecord := uuid.New()
	createdAt := uint64(time.Now().Unix())
	updatedAt := createdAt

	if _, err := pg.Pool.Exec(
		context.Background(),
		"INSERT INTO invoices (record, model, order, status, details, created_at, updated_at) VALUES ($1, $2, $3, $4, $5, $6, $7)",
		invoiceRecord, invoiceModel, orderRecord, 0, details, createdAt, updatedAt)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	// return c.SendStatus(200)
	return c.SendString(string(body))
}