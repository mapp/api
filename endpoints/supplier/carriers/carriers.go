package carrier

import (
	// Standard packages
	"context"
	"time"

	// Remote packages
	"github.com/alexedwards/argon2id"
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"

	// Local packages
	"api/helpers/functions/logger"
	"api/helpers/functions/sifter"
	env "api/helpers/variables/environment"

	// "api/helpers/functions/concat"
	"api/database/models/orders"
	"api/database/models/users"
	pg "api/database/postgres/connection"
)

type NonSensitiveCarrier struct {
	Data   users.NonSensitiveEmployee `json:"data"`
	Orders []orders.SupplierOrder     `json:"orders"`
}

type Carrier struct {
	Data   users.Employee         `json:"data"`
	Orders []orders.SupplierOrder `json:"orders"`
	Alias  string                 `json:"alias"`
}

type Seller struct {
	Data users.Employee `json:"data"`
}

// type Current struct {
// 	OrderIndex        uint64 `json:"orderIndex"`
// 	TransportationReadyItems       []bool `json:"loadedItems"`
// 	DistanceConfirmed bool   `json:"distanceConfirmed"`
// }
// type CarrierOrdersData struct {
// 	Current Current     `json:"current"`
// 	Book    interface{} `json:"book"`
// }

// Parses the payload struct data, querying the carrier (taxpayer), from the user's (alias) employee column,
// and compares the payload's passphrase with the hash retrieved using the Argon2 function to
// continue querying the rest needed for starting up the carrier panels correctly
func AuthenticateCarrier(c *fiber.Ctx) error {
	var carrier users.EmployeesAuthorizationRequest
	if err := c.BodyParser(&carrier); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// Executes the database query to get the supplier uuid and carrier entry (account) data
	var supplier string
	var account Carrier // Carrier account data
	// sql := concat.Join("SELECT supplier FROM users WHERE alias = '", carrier.Alias, "' AND data @> '{\"employees\":{\"carriers\":[{\"taxpayer\": ", carrier.taxpayer, ", \"phone\": ", carrier.Phone, "}]}}'")
	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT
			"user",
			(
				SELECT * FROM
					jsonb_array_elements(employees->'carriers')
				AS
					elem
				WHERE
					(elem->>'taxpayer') = $1
			)
		AS
			carrier
		FROM
			users
		WHERE
			alias = $2
		AND EXISTS
		(
			SELECT 1 FROM
				jsonb_array_elements(employees->'carriers')
			AS
				elem
			WHERE
				(elem->>'taxpayer') = $1
		)`,
		carrier.Taxpayer, carrier.Alias).Scan(&supplier, &account.Data)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// Compares the passphrase received by the login page to the passphrase hash stored in the
	// carrier's database entry, continuing to query the orders from it if all good or returning
	// an error if passphrases don not match each other
	match, err := argon2id.ComparePasswordAndHash(carrier.Passphrase, account.Data.Passphrase)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	if match { // If hash is empty it means the supplier is new and should set a new passphrase (At the "Account" page)
		account.Orders = []orders.SupplierOrder{}
		// Executes the database query to retrieve all orders from the supplier associated with the logged in carrier
		rows, _ := pg.Pool.Query(
			context.Background(),
			// "SELECT users.data AS customers_data, orders.record, orders.created_at, orders.updated_at, orders.user, orders.data AS orders_data FROM users INNER JOIN orders ON users.user = orders.customer WHERE orders.data->'delivery'->>'carrier' = $1",
			`SELECT
				users.data AS customers_data,
				orders.record,
				orders.created_at,
				orders.updated_at,
				orders.supplier,
				orders.data AS orders_data
			FROM
				customers
			RIGHT JOIN 
				orders ON users.user = orders.customer
			WHERE EXISTS
			(
				SELECT 1 FROM
					jsonb_array_elements(orders.data->'delivery'->'carriers')
				AS
					elem
				WHERE
					(elem->>'taxpayer') = $1
			)`,
			carrier.Taxpayer)

		for rows.Next() {
			var order orders.SupplierOrder
			err := rows.Scan(&order.CustomerData, &order.Record, &order.CreatedAt, &order.UpdatedAt, &supplier, &order.OrderData)
			if err != nil {
				logger.Error.Println(err)
				return c.SendStatus(500)
			}
			account.Orders = append(account.Orders, order)
		}

		// Creates the authentication token for the carrier agent to use the platform
		token := jwt.New(jwt.SigningMethodHS256)
		// Set claims
		claims := token.Claims.(jwt.MapClaims)
		claims["user"] = supplier
		claims["taxpayer"] = carrier.Taxpayer
		claims["exp"] = time.Now().Add(time.Hour * 72).Unix()
		// Generate encoded token and send it as response
		encodedToken, err := token.SignedString(env.JwtSigningKey)
		if err != nil {
			logger.Error.Println(err)
			return c.SendStatus(500)
		}

		account.Alias = carrier.Alias

		var response users.AuthorizationResponse
		response.Token = &encodedToken
		response.Payload = &account
		return c.JSON(response)
	} else {
		return c.SendStatus(404)
		// var r response.Default
		// r.Error = "ba1"
		// c.JSON(r)
	}
}

// Parses the payload struct data, querying the carrier (taxpayer), from the user's (alias) employee column,
// verifies the code received with the one previously stored on Redis using the phone as the key and given
// the codes matched each other, the current carrier's passphrase hash is replaced by the Argon2fied result of the payload's passphrase field
func UpdateCarrierPassphrase(c *fiber.Ctx) error {
	var payload users.EmployeesPassphraseUpdateRequest

	if err := c.BodyParser(&payload); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	var supplier string
	var account Carrier // Seller account data

	// Executes the database query to retrieve the
	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT
			"user",
			(
				SELECT * FROM
					jsonb_array_elements(employees->'carriers')
				AS
					elem
				WHERE
					(elem->>'taxpayer') = $1
			)
		AS
			carrier
		FROM
			users
		WHERE
			alias = $2
		AND EXISTS
		(
			SELECT 1 FROM
				jsonb_array_elements(employees->'carriers')
			AS
				elem
			WHERE
				(elem->>'taxpayer') = $1
		)`,
		payload.Taxpayer, payload.Alias).Scan(&supplier, &account.Data)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(404)
	}

	// Code verification
	isVerified, err := sifter.VerifyCode(account.Data.Phone, payload.Code)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	if !isVerified {
		return c.SendStatus(404)
	}

	// Creates a new hash out from the unhashed passphrase written by the carrier in the recovery page
	hash, err := argon2id.CreateHash(payload.Passphrase, argon2id.DefaultParams)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	updatedAt := uint64(time.Now().Unix())
	// Executes the database query to update the given carrier entry (updated at and new passphrase) within the supplier employees
	if _, err := pg.Pool.Exec(
		context.Background(),
		// https://stackoverflow.com/questions/66015250/update-object-field-of-element-in-array-jsonb-with-postgres
		`WITH j AS
		(
		 SELECT ('{carriers,'||idx-1||',passphrase}')::text[] AS path, j
		   FROM 
			  users 
		  CROSS JOIN 
		  JSONB_ARRAY_ELEMENTS(employees->'carriers') 
		   WITH ORDINALITY 
			  arr(j,idx)
		  WHERE 
			  j->>'taxpayer'= $1
		)
		UPDATE users
		   SET 
			   employees = JSONB_SET(employees,j.path, to_jsonb($2::text),false), updated_at= $3
		  FROM 
			  j
			WHERE 
				alias = $4
		`,
		payload.Taxpayer, hash, updatedAt, payload.Alias); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	return c.SendStatus(200)
}

func SelectExistsCarrierOrSeller(c *fiber.Ctx) error {
	// Parse attempt being empty causes panic
	taxpayer := c.Params("identifier")
	if taxpayer == "" {
		return c.SendStatus(400)
	}

	alias := c.Params("alias")
	if alias == "" {
		return c.SendStatus(400)
	}

	// Executes the database query
	var carrier Carrier
	var seller Seller
	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT
		(
			SELECT * FROM
				jsonb_array_elements(employees->'carriers')
			AS
				elem
			WHERE
				(elem->>'taxpayer') = $1
		),
		(
			SELECT * FROM
				jsonb_array_elements(employees->'sellers')
			AS
				elem
			WHERE
				(elem->>'taxpayer') = $1 
		)
	FROM
		users
	WHERE
		alias = $2
	`,
		taxpayer, alias).Scan(&carrier.Data, &seller.Data)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(404)
	}

	if !carrier.Data.Active && !seller.Data.Active {
		logger.Error.Println(carrier.Data.Active)
		return c.SendStatus(404)
	}

	sellerHasPassphrase := false
	carrierHasPassphrase := false

	if carrier.Data.Passphrase != "" && carrier.Data.Active {
		carrierHasPassphrase = true
	}
	if seller.Data.Passphrase != "" && seller.Data.Active {
		sellerHasPassphrase = true
	}

	// Returns the retrieved data
	return c.JSON(fiber.Map{
		"carrierActive":        carrier.Data.Active,
		"sellerActive":         seller.Data.Active,
		"carrierHasPassphrase": carrierHasPassphrase,
		"sellerHasPassphrase":  sellerHasPassphrase,
	})
}

type verifyCodeByTaxpayerAndAlias struct {
	Taxpayer string `json:"taxpayer"`
	Alias    string `json:"alias"`
}

func VerifyUserPhoneByTaxpayer(c *fiber.Ctx) error {
	var payload verifyCodeByTaxpayerAndAlias
	var employee users.Employee

	if err := c.BodyParser(&payload); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	err := pg.Pool.QueryRow(
	context.Background(),
	`SELECT
	(
		SELECT * FROM
			jsonb_array_elements(employees->'carriers')
		AS
			elem
		WHERE
			(elem->>'taxpayer') = $1
	)
	FROM
		users
	WHERE
		alias = $2
	`,
		payload.Taxpayer, payload.Alias).Scan(&employee)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(404)
	}

	if err := sifter.SendCode(employee.Phone); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	return c.SendStatus(200)
}

func SelectCarrierOrders(c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)
	taxpayer := claims["taxpayer"].(string)

	var account Carrier
	account.Orders = []orders.SupplierOrder{}
	// Executes the database query to retrieve the carrier orders
	rows, _ := pg.Pool.Query(
		context.Background(),
		// "SELECT customers.data AS customers_data, orders.record, orders.created_at, orders.updated_at, orders.supplier, orders.data AS orders_data FROM customers INNER JOIN orders ON customers.customer = orders.customer WHERE orders.supplier = $1 AND orders.data->'delivery'->>'carrier' = $2",
		`SELECT
			users.data AS customers_data,
			orders.record,
			orders.created_at,
			orders.updated_at,
			orders.supplier,
			orders.data AS orders_data
		FROM
			users
		RIGHT JOIN
			orders
		ON
			users.user = orders.customer
		WHERE
			orders.supplier = $1
		AND EXISTS
		(
			SELECT 1 FROM
				jsonb_array_elements(orders.data->'delivery'->'carriers')
			AS
				elem
			WHERE
				(elem->>'taxpayer') = $2
		)`,
		user, taxpayer)

	for rows.Next() {
		var order orders.SupplierOrder
		err := rows.Scan(&order.CustomerData, &order.Record, &order.CreatedAt, &order.UpdatedAt, &user, &order.OrderData)
		if err != nil {
			logger.Error.Println(err)
			return c.SendStatus(500)
		}
		account.Orders = append(account.Orders, order)
	}

	// Executes the database query to get the carrier account
	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT
			(
				SELECT * FROM
					jsonb_array_elements(employees->'carriers')
				AS
					elem
				WHERE
					(elem->>'taxpayer') = $1
			) AS carrier,
			alias
		FROM
			users
		WHERE
			"user" = $2`,
		taxpayer, user).Scan(&account.Data, &account.Alias)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	return c.JSON(account)
}

func UpdateCarrierOrder(c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	supplier := claims["user"].(string)
	// Parses the body data
	var order orders.Order // This struct stores the entire JSON
	if err := c.BodyParser(&order); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	order.Supplier = supplier
	// Updates the Unix timestamp
	order.UpdatedAt = uint64(time.Now().Unix())

	// Executes the database query
	if _, err := pg.Pool.Exec(
		context.Background(),
		`UPDATE
			orders
		SET
			updated_at = $1, data = $2
		WHERE
			"user" = $3
		AND
			record = $4`,
		order.UpdatedAt, order.Data, order.Supplier, order.Record); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	return c.JSON(fiber.Map{
		"updatedAt": order.UpdatedAt,
	})
}
