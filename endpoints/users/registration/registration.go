// Minified table configuration
// customer PRIMARY KEY NOT NULL, details JSONB, active BOOLEAN NOT NULL, key TEXT NOT NULL UNIQUE, passphrase TEXT NOT NULL, data JSON

package registration

import (
	// Standard packages
	"context"
	"time"

	// Remote packages
	"github.com/alexedwards/argon2id"
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"

	// Local packages
	"api/database/models/relationships"
	"api/database/models/users"
	pg "api/database/postgres/connection"
	"api/helpers/functions/concat"
	"api/helpers/functions/logger"
	"api/helpers/functions/sifter"
	env "api/helpers/variables/environment"
	// "api/models/response"
)

func SelectExistsUser (c *fiber.Ctx) error {
	// Customer key & identifier parse attempt being empty causes panic
	field := c.Params("identifierField")
	value := c.Params("identifierValue")
	if field == "" || value == "" {
		return c.SendStatus(400)
	}

	// Executes the database query
	var active, isSupplier bool
	sql := concat.Join(`
		SELECT
			active, is_supplier
		FROM
			users
		WHERE `, field, ` = $1`)
	err := pg.Pool.QueryRow(
		context.Background(),
		sql,
		value).Scan(&active, &isSupplier)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(404)
	}

	// Returns the retrieved data
	return c.JSON(fiber.Map{
		"active": active,
		"isSupplier": isSupplier,
	})
}

func SelectExistsUserAlias (c *fiber.Ctx) error {
	// Parse attempt being empty causes panic
	alias := c.Params("alias"); if alias == "" {
		return c.SendStatus(400)
	}

	// Executes the database query
	var verified bool
	err := pg.Pool.QueryRow(
		context.Background(),
		"SELECT EXISTS(SELECT 1 FROM users WHERE alias = $1)",
		alias).Scan(&verified)
	if err != nil || verified == false {
		logger.Error.Println(err)
		return c.SendStatus(404)
	}

	return c.SendStatus(200)
}

func SelectUnactivatedUserTaxpayerData (c *fiber.Ctx) error {
	// Parse attempt being empty causes panic
	taxpayer := c.Params("taxpayer"); if taxpayer == "" {
		return c.SendStatus(400)
	}

	logger.Error.Println(taxpayer)
	// Executes the database query
	var data users.Data
	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT data FROM users WHERE taxpayer = $1 AND active = $2`,
		taxpayer, false).Scan(&data)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(404)
	}

	// Returns the retrieved data
	return c.JSON(fiber.Map{
		"data": data,
	})
}

func SendCodeToPhone (c *fiber.Ctx) error {
	var payload users.E123Phone

	if err := c.BodyParser(&payload); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	if err := sifter.SendCode(payload); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	return c.SendStatus(200)
}

func SendCodeToUserTaxpayerPhone (c *fiber.Ctx) error {
	payload := struct {
		Taxpayer string `json:"taxpayer"`
	}{}

	if err := c.BodyParser(&payload); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	var data users.Data
	err := pg.Pool.QueryRow(
		context.Background(),
		"SELECT data FROM users WHERE taxpayer = $1",
		payload.Taxpayer).Scan(&data)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(404)
	}

	if err := sifter.SendCode(data.Administrative.Reference.Phone); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	return c.SendStatus(200)
}

// Used for registration of new unactivated users
type unactivatedUser struct {
	Taxpayer   string           `json:"taxpayer"`
	Data       users.Data       `json:"data"`
	Background users.Background `json:"background"`
}
type unactivatedUserRequest struct {
	User unactivatedUser `json:"user"`
	Relationship relationships.Relationship `json:"relationship"`
}
// Inserts a new user account that is active and able to login by itself
func InsertUnactivatedUser (c *fiber.Ctx) error {

	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	reference := claims["user"].(string)

	logger.Error.Println(c.Locals("authorization").(*jwt.Token).Claims)

	var payload unactivatedUserRequest // This struct stores the entire JSON
	if err := c.BodyParser(&payload); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	target    := uuid.New()
	createdAt := uint64(time.Now().Unix())
	updatedAt := createdAt

	score := 10000

	if _, err := pg.Pool.Exec(
		context.Background(),
		`INSERT INTO
			users ("user", created_at, updated_at, active, taxpayer, data, background, is_supplier, alias, score)
		VALUES
			($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
		`,
		target, createdAt, updatedAt, false, payload.User.Taxpayer, payload.User.Data, payload.User.Background, false, payload.User.Data.Administrative.Byname, score)
		err != nil {
		logger.Error.Println(err);
		return c.SendStatus(400)
	}

	if _, err := pg.Pool.Exec(
		context.Background(),
		`INSERT INTO
			relationships (created_at, updated_at, reference, target, relation, payables, receivables)
		VALUES
			($1, $2, $3, $4, $5, $6, $7)
		`,
		createdAt, updatedAt, reference, target, payload.Relationship.Relation, payload.Relationship.Situation.Payables, payload.Relationship.Situation.Receivables )
		err != nil {
		logger.Error.Println(err);
		return c.SendStatus(400)
	}

	return c.JSON(payload.Relationship)
}

type employeesStruct struct {
	Carriers []string `json:"carriers"`
	Sellers  []string `json:"sellers"`
}


// Used for registration of new activated users
type activatedUser struct {
	Taxpayer   string          `json:"taxpayer"`
	Passphrase string          `json:"passphrase"`
	IsSupplier bool            `json:"isSupplier"`
	Alias      string          `json:"alias"`
	Data       users.Data      `json:"data"`
	Employees  employeesStruct `json:"employees"`
}
type activatedUserRequest struct {
	User activatedUser `json:"user"`
	Code string        `json:"code"`
}
// Inserts a new user account that is not active and is not able to login by itself, needs to complete registration afterwards
func InsertActivatedUser (c *fiber.Ctx) error {
	var payload activatedUserRequest // This struct stores the entire JSON
	if err := c.BodyParser(&payload); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// Code verification
	isVerified, err := sifter.VerifyCode(payload.User.Data.Administrative.Reference.Phone, payload.Code); if err != nil {
		// isVerified, err := sifter.VerifyCode("15148202705", access.Code); if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	if isVerified == false {
		return c.SendStatus(404)
	}

	hash, err := argon2id.CreateHash(payload.User.Passphrase, argon2id.DefaultParams)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}


	user      := uuid.New()
	createdAt := uint64(time.Now().Unix())
	updatedAt := createdAt
	score := 10000
	if _, err := pg.Pool.Exec(
		context.Background(),
		// Inserts or updates a new user appropriately (Assumes an unique index has been defined that constrains values appearing in the taxpayer column)
		`INSERT INTO
			users ("user", created_at, "updated_at", active, taxpayer, passphrase, data, is_supplier, alias, score, employees)
		VALUES
			($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
		ON CONFLICT
			(taxpayer)
		DO UPDATE SET
			updated_at = $3, active = $4, passphrase = $6, data = $7
		`,
		user, createdAt, updatedAt, true, payload.User.Taxpayer, hash, payload.User.Data, payload.User.IsSupplier, payload.User.Alias, score, payload.User.Employees)
		err != nil {
		logger.Error.Println(err);
		return c.SendStatus(400)
	}

	// Create token
	token := jwt.New(jwt.SigningMethodHS256)
	// Set claim
	claims := token.Claims.(jwt.MapClaims)
	claims["user"] = user
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()
	// Generate encoded token and send it as response
	encodedToken, err := token.SignedString(env.JwtSigningKey)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(500)
	}

	var sd users.AuthorizationResponse
	sd.Token = &encodedToken
	sd.Payload = &payload.User.Data
	sd.Status = sifter.SetCustomerStatusOnKey(payload.User.Data.Administrative.Reference.Phone)

	// sd.Status = 1

	// if (sd.Status == 1) { // 0 -> Ok, 1 -> Not allowed in beta
	// 	return c.JSON(sd)
	// }

	return c.JSON(sd)
}

func SelectUser (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	customer := claims["customer"].(string)

	// Executes the database query
	user := struct {
		Active     bool        `json:"active"`
		Taxpayer   string      `json:"taxpayer"`
		Alias      string      `json:"alias"`
		Data       users.Data  `json:"data"`
		Membership interface{} `json:"membership"`
	}{}
	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT
			active, taxpayer, alias, data, membership
		FROM
			users
		WHERE
			"user" = $1`,
		customer).Scan(&user.Active, &user.Taxpayer, &user.Alias, &user.Data, &user.Membership)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	// Returns the retrieved data
	return c.JSON(user)
}

func UpdateUserPassphrase (c *fiber.Ctx) error {
	payload := struct {
		Taxpayer   string  `json:"taxpayer"`
		Code       string  `json:"code"`
		Passphrase string  `json:"passphrase"`
	}{} // This struct stores the entire JSON

	var data users.Data

	if err := c.BodyParser(&payload); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	err := pg.Pool.QueryRow(
		context.Background(),
		"SELECT data FROM users WHERE taxpayer = $1",
		payload.Taxpayer).Scan(&data)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(404)
	}

	// Code verification
	isVerified, err := sifter.VerifyCode(data.Administrative.Reference.Phone, payload.Code); if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	if isVerified == false {
		return c.SendStatus(404)
	}

	hash, err := argon2id.CreateHash(payload.Passphrase, argon2id.DefaultParams)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	updatedAt := uint64(time.Now().Unix())

	// Executes the database query
	if _, err := pg.Pool.Exec(
		context.Background(),
		"UPDATE users SET updated_at = $1, passphrase = $2 WHERE taxpayer = $3",
		updatedAt, hash, payload.Taxpayer)
		err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	return c.SendStatus(200)
}

func SelectUnactivatedUser(c *fiber.Ctx) error {
	// Parse attempt being empty causes panic
	taxpayer := c.Params("taxpayer")
	if taxpayer == "" {
		return c.SendStatus(400)
	}

	logger.Error.Println(taxpayer)
	// Executes the database query

	var data users.Data
	var background users.Background
	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT data, background FROM users WHERE taxpayer = $1 AND active = $2`,
		taxpayer, false).Scan(&data, &background)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(404)
	}

	// Returns the retrieved data
	return c.JSON(fiber.Map{
		"background": background,
		"data":       data,
	})
}