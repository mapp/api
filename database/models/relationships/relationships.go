// Minified table configuration
// reference UUID NOT NULL,
// target UUID NOT NULL,
// relation SMALLINT NOT NULL,
// payables JSONB,
// receivables JSONB,
// created_at BIGINT NOT NULL,
// updated_at BIGINT NOT NULL

package relationships

import (
	"api/database/models/orders"
)

type Accounts struct {
	CreatedAt uint64         `json:"createdAt"`
	UpdatedAt uint64         `json:"updatedAt"`
	Payment   orders.Payment `json:"payment"`
}
type Situation struct {
	Payables    []Accounts `json:"payables"`    // Payable accounts from the reference user to the target user
	Receivables []Accounts `json:"receivables"` // Payable accounts from the target user to the referenrce use
}
type Relationship struct {
	Company   string    `json:"company"`
	Byname    string    `json:"byname"`
	Taxpayer  string    `json:"taxpayer"`
	Name      string    `json:"name"`
	Surname   string    `json:"surname"`
	Relation  uint8     `json:"relation"`
	Situation Situation `json:"situation"`
	// Credit      Credit    `json:"credit"`
}
