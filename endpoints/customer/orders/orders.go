// Minified table configuration
// record BIGSERIAL PRIMARY KEY NOT NULL, created_at BIGINT NOT NULL, updated_at BIGINT NOT NULL, customer BIGINT NOT NULL, supplier BIGINT NOT NULL, data JSONB NOT NULL

package orders

import (
	// Standard packages
	"context"
	"time"
	// "encoding/json"

	// Remote packages
	"github.com/golang-jwt/jwt/v4"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	
	// Local packages
	"api/helpers/functions/logger"
	pg "api/database/postgres/connection"
	"api/database/models/items"
	"api/database/models/orders"
	// "api/services/aws/s3"
	// "api/helpers/functions/concat"
)

// Returns an instance of ConfirmedOrderDetails struct once the new order gets successfully inserted into the database
func InsertCustomerOrder (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	// user := claims["user"].(string)
	// String when the customer is present (registered user) or nil for guests (unregistered user)
	customer := claims["customer"]

	var order orders.Order // This struct stores the entire JSON
	if err := c.BodyParser(&order.Data); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// Executes the database query to get the supplier uuid
	if err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT
			"user", data->'shipping'
		FROM
			users
		WHERE
			alias = $1`,
		order.Data.Alias).Scan(&order.Supplier, &order.Data.Delivery.Shipping); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// Subtracts the amount of each article in the order content, from the supplier warehouse
	for i := range order.Data.Content {
		element := &order.Data.Content[i] // References the array's element
		item := element.Item

		if item.Active == false { // The item have to be active, impossible to operate
			return c.SendStatus(500)
		}

		// Only makes sense to update the item quantity if the item is ready available and does not need to be (back) ordered by the supplier
		if item.Data.Control.Available == true {

			quantityLeft := item.Data.Control.Quantity - element.Amount
			// This approach is just a bit more costly, considering that is unlikely that the quantity left will be zero for most of the operations
			// To solve that by the way would require a more complex algorithm, probably less readable
			if quantityLeft < 0 { // Impossible to operate
				return c.SendStatus(500)
			} else { // If not negative the quantity is zero or it is positive
				item.Data.Control.Quantity = quantityLeft // Updates the item quantity
				//==========
				if item.Data.Discount.Quota > 0 { // Check if there is a discount quota
					quotaLeft := item.Data.Discount.Quota - element.Amount
					if quotaLeft == 0 {
						item.Data.Discount = items.Discount{item.Data.Discount.Custom, 0, 0} // Cancels the discount and resets the values
					} else if quotaLeft > 0 {
						item.Data.Discount.Quota = quotaLeft // Updates the discount quota
					} else { // Negative quota left, impossible to operate
						return c.SendStatus(500)
					}
				}
				//==========
				active := true
				if quantityLeft == 0 && item.Data.Control.AutoDeactivatable == true { // Checks if the product just ran out of stock && is set for auto-deactivation
					active = false // De-activatates the active product
				}
				// Updates the quantity in the data and maybe the active status/the discount quota
				if _, err := pg.Pool.Exec(
					context.Background(),
					`UPDATE
						items
					SET
						active = $1, data = $2
					WHERE
						supplier = $3
					AND
						entry = $4`,
					active, item.Data, order.Supplier, item.Entry)
				err != nil {
					logger.Error.Println(err)
					return c.SendStatus(400)
				}
			}

		}
	}


	order.Customer = customer
	order.CreatedAt = uint64(time.Now().Unix())
	order.UpdatedAt = order.CreatedAt

	// JSON fixes
	// To make sure the value is stored as array and not null 
	order.Data.Delivery.Carriers = []orders.Carrier{} 

	record := uuid.New()
	// Executes the database query
	if _, err := pg.Pool.Exec(
		context.Background(),
		`INSERT INTO
			orders (record, created_at, updated_at, customer, supplier, data)
		VALUES
			($1, $2, $3, $4, $5, $6)
		RETURNING
			record`,
		record, order.CreatedAt, order.UpdatedAt, order.Customer, order.Supplier, order.Data)
	err != nil {
		// TODO!
		// IF ERROR HAPPENS, THE MODIFICATIONS SHOULD BE REVERSED! QUANTITY RESTORED, ETC
		logger.Error.Println(err);
		return c.SendStatus(400)
	}

	return c.JSON(fiber.Map{
        "record":    order.Record,
        "createdAt": order.CreatedAt,
        "updatedAt": order.UpdatedAt,
    })
}

// Retrieves all orders from a given customer primary key
func SelectCustomerOrders (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	customer := claims["user"].(string)
	// Executes the database query
	list := []orders.GeneralUsersOrder{}
	rows, _ := pg.Pool.Query(
		context.Background(),
		`SELECT
			record, created_at, updated_at, data
		FROM
			orders
		WHERE
			customer = $1`,
		customer)

	for rows.Next() {
		var nonSensitiveOrder orders.GeneralUsersOrder
		err := rows.Scan(&nonSensitiveOrder.Record, &nonSensitiveOrder.CreatedAt, &nonSensitiveOrder.UpdatedAt, &nonSensitiveOrder.Data)
		if err != nil {
			logger.Error.Println(err)
			return c.SendStatus(500)
		}

		list = append(list, nonSensitiveOrder)
	}

	// Returns the retrieved orders
	return c.JSON(list)
}

// // Updates the customer order status
// func UpdateCustomerOrderStatus (c *fiber.Ctx) error {
// 	// Parse attempt being nil causes panic
// 	if c.Locals("authorization") == nil {
// 		return c.SendStatus(500)
// 		return
// 	}
// 	// Dumps the content from the JWT token
// 	token := c.Locals("authorization").(*jwt.Token)
// 	claims := token.Claims.(jwt.MapClaims)
// 	user := claims["user"].(string)
// 	// Parses the body data
// 	var data users.Data // This struct stores the entire JSON
// 	if err := c.BodyParser(&data); err != nil {
// 		logger.Error.Println(err)
// 		return c.SendStatus(400)
// 		return
// 	}
// 	// Executes the database query
// 	if _, err := pg.Pool.Exec(
// 		context.Background(),
// 		"UPDATE customers SET alias = $1, data = $2 WHERE "user" = $3",
// 		data.Email, data, customer)
// 	err != nil {
// 		logger.Error.Println(err)
// 		return c.SendStatus(400)
// 		return
// 	}
// 	// Returns a success status
// 	return c.SendStatus(200)
// }