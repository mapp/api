package gatewayApi

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"

	"github.com/mrjones/oauth"

	env "api/helpers/variables/environment"
)

// Request
type GatewayAPIRecipient struct {
	Msisdn uint64 `json:"msisdn"`
}
type GatewayAPIRequest struct {
	Sender     string                `json:"sender"`
	Message    string                `json:"message"`
	Recipients []GatewayAPIRecipient `json:"recipients"`
}

func SendSms (sender string, message string, mobile uint64) error {
	// Authentication
	consumer := oauth.NewConsumer(env.GatewayApiKey, env.GatewayApiSecret, oauth.ServiceProvider{})
	client, err := consumer.MakeHttpClient(&oauth.AccessToken{})
	if err != nil {
		return err
		// log.Fatal(err)
	}

	request := &GatewayAPIRequest{
		Sender:  sender,
		Message: message,
		Recipients: []GatewayAPIRecipient{
			{
				Msisdn: mobile,
			},
		},
	}

	// Send it
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(request); err != nil {
		return err
		// log.Fatal(err)
	}
	res, err := client.Post(
		"https://gatewayapi.com/rest/mtsms",
		"application/json",
		&buf,
	)
	if err != nil {
		return err
		// log.Fatal(err)
	}
	if res.StatusCode != 200 {
	body, _ := ioutil.ReadAll(res.Body)
		log.Fatalf("http error reply, status: %q, body: %q", res.Status, body)
	}

	// Parse the response
	type GatewayAPIResponse struct {
		Ids []uint64
	}
	response := &GatewayAPIResponse{}
	if err := json.NewDecoder(res.Body).Decode(response); err != nil {
		return err
		// log.Fatal(err)
	}
	// log.Println("ids", response.Ids)

	return nil
}