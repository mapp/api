package s3

import (
	// Standard packages
	"context"
	"mime/multipart"
	"bytes"
	"path"
	"time"
	"fmt"
	"os"

	// Remote packages
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"

	"github.com/kenshaw/baseconv"

	// Local packages
	env "api/helpers/variables/environment"
	"api/helpers/functions/hasher"
	"api/helpers/functions/concat"
)

func NewSession () *s3.S3 {
	// All clients require a Session. The Session provides the client with
	// shared configuration such as region, endpoint, and credentials. A
	// Session should be shared where possible to take advantage of
	// configuration and credential caching. See the session package for
	// more information.
	sess := session.Must(session.NewSession(&aws.Config{
		Region: aws.String(env.AwsS3Region),
		Credentials: credentials.NewStaticCredentials(
			env.AwsAccessKeyId,     // Secret ID
			env.AwsSecretAccessKey, // Secret Key
			""),
	}))
	// Create a new instance of the service's client with a Session.
	// Optional aws.Config values can also be provided as variadic arguments
	// to the New function. This option allows you to provide service
	// specific configuration.
	svc := s3.New(sess)

	return svc	
}

func UploadFile (svc *s3.S3, file *multipart.FileHeader, location string) (string, error) {
	// A minute timeout for the upload session
	timeout := 60 * time.Second
	// Create a context with a timeout that will abort the upload if it takes
	// more than the passed in timeout.
	ctx := context.Background()
	var cancelFn func()
	if timeout > 0 {
		ctx, cancelFn = context.WithTimeout(ctx, timeout)
	}
	// Ensure the context is canceled to prevent leaking.
	// See context package for more information, https://golang.org/pkg/context/
	if cancelFn != nil {
		defer cancelFn()
	}

	// Opens and returns the associated File
	handler, err := file.Open(); if err != nil {
		return "", err
	}
	defer handler.Close()

	// Get the file size and read the file content into a buffer
	size := file.Size
	buffer := make([]byte, size)
	handler.Read(buffer)

	// Generates the image filename for upload based on CRC32 hash
	// basename := strconv.FormatUint(uint64(hashing.GetCrc32Sum(buffer)), 16); if err != nil {
	// 	return "", err
	// }

	basename, _ := baseconv.Convert(hasher.GetSha1SumHex(buffer), baseconv.DigitsHex, baseconv.Digits62)

	extension := path.Ext(file.Filename)
	filename := concat.Join(basename, extension)
	path := concat.Join(location, filename)

	// Uploads the object to S3. The Context will interrupt the request if the
	// timeout expires.
	_, err = svc.PutObjectWithContext(ctx, &s3.PutObjectInput{
		Bucket: aws.String(env.AwsS3BucketName),
		Key:    aws.String(path),
		ACL:    aws.String(env.AwsS3BucketAcl),
		Body:   bytes.NewReader(buffer),
	})
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok && aerr.Code() == request.CanceledErrorCode {
			// If the SDK can determine the request or retry delay was canceled
			// by a context the CanceledErrorCode error code will be returned.
			fmt.Fprintf(os.Stderr, "upload canceled due to timeout, %v\n", err)
		} else {
			fmt.Fprintf(os.Stderr, "failed to upload object, %v\n", err)
		}
	}

	return filename, nil
}

// DeleteItem deletes an item from a bucket
// Inputs:
//     sess is the current session, which provides configuration for the SDK's service clients
//     bucket is the name of the bucket
//     item is the name of the bucket object to delete
// Output:
//     If success, nil
//     Otherwise, an error from the call to DeleteObject or WaitUntilObjectNotExists
func DeleteFile (svc *s3.S3, path string) error {
	// A minute timeout for the delete session
	timeout := 60 * time.Second
	// Create a context with a timeout that will abort the delete if it takes
	// more than the passed in timeout.
	ctx := context.Background()
	var cancelFn func()
	if timeout > 0 {
		ctx, cancelFn = context.WithTimeout(ctx, timeout)
	}
	// Ensure the context is canceled to prevent leaking.
	// See context package for more information, https://golang.org/pkg/context/
	if cancelFn != nil {
		defer cancelFn()
	}

	_, err := svc.DeleteObjectWithContext(ctx, &s3.DeleteObjectInput{
		Bucket: aws.String(env.AwsS3BucketName),
		Key:    aws.String(path),
	})
	if err != nil {
		return err
	}

	err = svc.WaitUntilObjectNotExists(&s3.HeadObjectInput{
		Bucket: aws.String(env.AwsS3BucketName),
		Key:    aws.String(path),
	})
	if err != nil {
		return err
	}

	return nil
}