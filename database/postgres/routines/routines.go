package routines

import (
	// Standard packages
	"context"
	"os"

	// Local packages
	"api/helpers/functions/logger"
	pg "api/database/postgres/connection"
)

func ensureTableUsers () {
	if _, err := pg.Pool.Exec(
		context.Background(),
		`CREATE TABLE IF NOT EXISTS
			users
			(
				"user" UUID PRIMARY KEY,
				created_at BIGINT NOT NULL,
				updated_at BIGINT NOT NULL,
				passphrase TEXT,
				is_supplier BOOLEAN NOT NULL,
				active BOOLEAN NOT NULL,
				taxpayer TEXT UNIQUE,
				alias TEXT UNIQUE,
				score SMALLINT NOT NULL,
				"data" JSONB,
				background JSONB,
				membership JSONB,
				settings JSONB,
				employees JSONB
			)
		`)
	err != nil {
		logger.Error.Println(err)
		os.Exit(1)
	}
	logger.Info.Println("Users' table is up")
}

func ensureTableItems () {
	if _, err := pg.Pool.Exec(
		context.Background(),
		`CREATE TABLE IF NOT EXISTS
			items
			(
				item UUID PRIMARY KEY,
				created_at BIGINT NOT NULL,
				updated_at BIGINT NOT NULL,
				active BOOLEAN NOT NULL,
				entry INT NOT NULL,
				supplier UUID NOT NULL,
				source UUID,
				"data" JSONB
			)
		`)
	err != nil {
		logger.Error.Println(err)
		os.Exit(1)
	}

	// https://dba.stackexchange.com/questions/179598/how-can-i-use-a-full-text-search-on-a-jsonb-column-with-postgres
	// Creates a GIN index on the JSONB column to optimize future queries
	if _, err := pg.Pool.Exec(
		context.Background(),
		`CREATE INDEX
			item_description_search
		ON
			items
		USING
			gin(to_tsvector('portuguese', data->'control'->>'description'))
		`)
	err != nil {
		logger.Error.Println(err)
		// os.Exit(1)
	}

	// https://medium.com/@gabrielfgularte/caracteres-especiais-e-full-text-search-no-postgre-c345b1da5b7b
	// To enable special characters mainly present in latin languages
	if _, err := pg.Pool.Exec(
		context.Background(),
		`CREATE EXTENSION unaccent`)
	err != nil {
		logger.Error.Println(err)
		// os.Exit(1)
	}

	logger.Info.Println("Items' table is up")
}

func ensureTableOrders () {
	// Customer can be string (uuid) or nil
	// Supplier are confidential and should not be exposed outside the server
	if _, err := pg.Pool.Exec(
		context.Background(),
		`CREATE TABLE IF NOT EXISTS
			orders
			(
				record UUID PRIMARY KEY,
				created_at BIGINT NOT NULL,
				updated_at BIGINT NOT NULL,
				supplier UUID NOT NULL,
				customer UUID, 
				"data" JSONB NOT NULL
			)
		`)
	err != nil {
		logger.Error.Println(err)
		os.Exit(1)
	}
	logger.Info.Println("Orders' table is up")
}

func ensureTableInvoices () {
	if _, err := pg.Pool.Exec(
		context.Background(),
		`CREATE TABLE IF NOT EXISTS
			invoices
			(
				record UUID PRIMARY KEY,
				created_at BIGINT NOT NULL,
				updated_at BIGINT NOT NULL,
				model SMALLINT NOT NULL,
				status SMALLINT NOT NULL,
				details JSONB,
				"order" UUID[] NOT NULL
			)
		`)
	err != nil {
		logger.Error.Println(err)
		os.Exit(1)
	}
	logger.Info.Println("Invoices' table is up")
}

func ensureTableRelationships () {
	if _, err := pg.Pool.Exec(
		context.Background(),
		`CREATE TABLE IF NOT EXISTS
			relationships
			(
				created_at BIGINT NOT NULL,
				updated_at BIGINT NOT NULL,
				reference UUID NOT NULL,
				target UUID NOT NULL,
				relation SMALLINT NOT NULL,
				payables JSONB,
				receivables JSONB
			)
		`)
	err != nil {
		logger.Error.Println(err)
		os.Exit(1)
	}
	logger.Info.Println("Relationships' table is up")
}

func ensureTableEvents () {
	if _, err := pg.Pool.Exec(
		context.Background(),
		`CREATE TABLE IF NOT EXISTS
			events_prelaunch
			(
				entry BIGSERIAL PRIMARY KEY,
				company TEXT NOT NULL,
				email TEXT NOT NULL
			)
		`)
	err != nil {
		logger.Error.Println(err)
		os.Exit(1)
	}
	logger.Info.Println("Events' table is up")
}

func EnsureTables () {
	// Executes the database queries
	ensureTableUsers()
	ensureTableItems()
	ensureTableOrders()
	ensureTableEvents()
	ensureTableInvoices()
	ensureTableRelationships()
	logger.Info.Println("All tables are running")
}