// Minified table configuration
// record UUID NOT NULL, model SMALLINT NOT NULL, status SMALLINT NOT NULL, details JSONB, "order" UUID NOT NULL, created_at BIGINT NOT NULL, updated_at BIGINT NOT NULL

// Tipos de nota fiscal:

// 0- NF-e Nota Fiscal Eletrônica de Produtos ou Mercadorias)
// 1- CT-e – Conhecimento de Transporte Eletrônico
// 2- NFS-e (Nota Fiscal Eletrônica de Serviços)
// 3- NFC-e (Nota Fiscal ao Consumidor Eletrônica)
// 4- Cupom Fiscal Eletrônico (CF-e)
// 5- Módulo Fiscal Eletrônico (MF-e)
// 6- MDF-e – Manifesto de Documentos Fiscais Eletrônicos
// 7- Nota Fiscal Avulsa (NFAe)
// 8- Nota Fiscal Complementar
// 9- Nota Fiscal Denegada
// 10- Nota Fiscal Rejeitada
// 11- Nota Fiscal de Exportação
// 12- Nota Fiscal de Remessa

// Status referentes à autorização da nota fiscal:

// 0- AguardandoAutorizacao (Status inicial da nota fiscal)
// 1- SolicitandoAutorizacao (Nota fiscal sendo incluída na fila de transmissão)
// 2- AutorizacaoSolicitada (Nota fiscal incluída na fila de transmissão)
// 3- EmProcessoDeAutorizacao (Sua nota fiscal foi transmitida e o Gateway está aguardando um retorno)
// 4- AutorizadaAguardandoGeracaoPDF (Sua nota foi autorizada e está na fila de geração de PDF)
// 5- Autorizada (wh) (Nota fiscal autorizada e com PDF pronto para download)
// 6- Negada (wh) (A nota fiscal foi rejeitada pelo órgão)

// Status referentes ao cancelamento da nota fiscal:

// 7- SolicitandoCancelamento (Nota fiscal sendo incluída na fila de cancelamento)
// 8- CancelamentoSolicitado (Nota fiscal incluída na fila de cancelamento)
// 9- EmProcessoDeCancelamento (wh) (Cancelamento solicitado e o Gateway está aguardando um retorno)
// 10- CanceladaAguardandoAtualizacaoPDF (Nota fiscal cancelada pela prefeitura e está na fila para atualização de seu PDF)
// 11- Cancelada (wh) (Nota fiscal cancelada e com PDF atualizado pronto para download)
// 12- CancelamentoNegado (wh) (O cancelamento da nota fiscal foi rejeitado)

// Only invoices (NFC-e/NF-e) emitted by a paying supplier on Materiapp (different from the suppliers that are registered but are not active/paying)
package invoices

type BrazilianInvoice struct {
	Record    string      `json:"record"`
	Model     uint8       `json:"type"` // "Tipos"
	Status    uint8       `json:"status"`
	Details   interface{} `json:"details"`
	Order     string      `json:"order"` // Reference order
	CreatedAt uint64      `json:"createdAt"`
	UpdatedAt uint64      `json:"updatedAt"`
}

type CredenciadoraCartao struct {
	TipoIntegracaoPagamento string `json:"tipoIntegracaoPagamento"`
	CnpjCredenciadoraCartao string `json:"cnpjCredenciadoraCartao"`
	Bandeira                string `json:"bandeira"`
	Autorizacao             string `json:"autorizacao"`
}

type Formas struct {
	TipoFormas          string              `json:"tipo"`
	Valor               float32             `json:"valor"`
	CredenciadoraCartao CredenciadoraCartao `json:"credenciadoraCartao"`
}

type Pagamento struct {
	TipoPagamento string   `json:"tipo"`
	Formas        []Formas `json:"formas"`
}

type Pedido struct {
	PresencaConsumidor string    `json:"presencaConsumidor"`
	Pagamento          Pagamento `json:"pagamento"`
}

type Simplificado struct {
	Percentual int8 `json:"percentual"`
}

type Detalhado struct {
	ṔercentualFederal   int8 `json:"percentualFederal"`
	PercentualEstadual  int8 `json:"percentualEstadual"`
	PercentualMunicipal int8 `json:"percentualMunicipal"`
}

type PercentualAproximadoTributos struct {
	Simplificado Simplificado `json:"simplificado"`
	Detalhado    Detalhado    `json:"detalhado"`
	Fonte        string       `json:"fonte"`
}

type Icms struct {
	SituacaoTributaria                string  `json:"situacaoTributaria"`
	Origem                            int8    `json:"origem"`
	Aliquota                          float32 `json:"aliquota"`
	BaseCalculo                       float32 `json:"baseCalculo"`
	ModalidadeBaseCalculo             int8    `json:"modalidadeBaseCalculo"`
	PercentualReducaoBaseCalculo      float32 `json:"percentualReducaoBaseCalculo"`
	BaseCalculoST                     float32 `json:"baseCalculoST"`
	AliquotaST                        int8    `json:"aliquotaST"`
	ModalidadeBaseCalculoST           int8    `json:"modalidadeBaseCalculoST"`
	PercentualReducaoBaseCalculoST    float32 `json:"percentualReducaoBaseCalculoST"`
	PercentualMargemValorAdicionadoST float32 `json:"percentualMargemValorAdicionadoST"`
}

type PorAliquota struct {
	Aliquota float32 `json:"aliquota"`
}

type Pis struct {
	SituacaoTributaria string      `json:"situacaoTributaria"`
	PorAliquota        PorAliquota `json:"porAliquota"`
}

type Cofins struct {
	SituacaoTributaria string      `json:"situacaoTributaria"`
	PorAliquota        PorAliquota `json:"porAliquota"`
}

type Ipi struct {
	SituacaoTributaria string      `json:"situacaoTributaria"`
	PorAliquota        PorAliquota `json:"porAliquota"`
}

type Impostos struct {
	PercentualAproximadoTributos PercentualAproximadoTributos `json:"percentualAproximadoTributos"`
	Icms                         Icms                         `json:"icms"`
	Pis                          Pis                          `json:"pis"`
	Cofins                       Cofins                       `json:"cofins"`
	Ipi                          Ipi                          `json:"ipi"`
}

type Cliente struct {
	Endereco                  Endereco `json:"endereco"`
	TipoPessoa                string   `json:"tipoPessoa"`
	Nome                      string   `json:"nome"`
	Email                     string   `json:"email"`
	CpfCnpj                   string   `json:"cpfCnpj"`
	InscricaoMunicipal        string   `json:"inscricaoMunicipal"`
	InscricaoEstadual         string   `json:"inscricaoEstadual"`
	IndicadorContribuinteICMS string   `json:"indicadorContribuinteICMS"`
	Telefone                  string   `json:"telefone"`
}

type Itens struct {
	Cfop           string   `json:"cfop"`
	Codigo         string   `json:"codigo"`
	Descricao      string   `json:"descricao"`
	Ncm            string   `json:"ncm"`
	Cest           string   `json:"cest"`
	Quantidade     string   `json:"quantidade"`
	UnidadeMedida  string   `json:"unidadeMedida"`
	ValorUnitario  string   `json:"valorUnitario"`
	Desconto       string   `json:"desconto"`
	OutrasDespesas string   `json:"outrasDespesas"`
	Impostos       Impostos `json:"impostos"`
}

type NfeReferida struct {
	ChaveAcesso string `json:"chaveAcesso"`
}

type Frete struct {
	Modalidade string `json:"modalidade"`
}

type EnderecoEntrega struct {
	TipoPessoaDestinatario string `json:"tipoPessoaDestinatario"`
	CpfCnpjDestinatario    string `json:"cpfCnpjDestinatario"`
	Pais                   string `json:"pais"`
	Uf                     string `json:"uf"`
	Cidade                 string `json:"cidade"`
	Logradouro             string `json:"logradouro"`
	Numero                 string `json:"numero"`
	Complemento            string `json:"complemento"`
	Bairro                 string `json:"bairro"`
	Cep                    string `json:"cep"`
}

type Transportadora struct {
	UsarDadosEmitente bool   `json:"usarDadosEmitente"`
	TipoPessoa        string `json:"tipoPessoa"`
	CpfCnpj           string `json:"cpfCnpj"`
	Nome              string `json:"nome"`
	InscricaoEstadual string `json:"inscricaoEstadual"`
	EnderecoCompleto  string `json:"enderecoCompleto"`
	Uf                string `json:"uf"`
	Cidade            string `json:"cidade"`
}

type Veiculo struct {
	Placa string `json:"placa"`
	Uf    string `json:"uf"`
}

type Volume struct {
	Quantidade  float32 `json:"quantidade"`
	Especie     string  `json:"especie"`
	Marca       string  `json:"marca"`
	Numeracao   string  `json:"numeracao"`
	PesoBruto   float32 `json:"pesoBruto"`
	PesoLiquido float32 `json:"pesoLiquido"`
}

type Transporte struct {
	Frete           Frete           `json:"frete"`
	EnderecoEntrega EnderecoEntrega `json:"enderecoEntrega"`
	Transportadora  Transportadora  `json:"transportadora"`
	Veiculo         Veiculo         `json:"veiculo"`
	Volume          Volume          `json:"volume"`
}

type NFCinvoiceEmission struct {
	Id               string      `json:"id"`
	AmbienteEmissao  string      `json:"ambienteEmissao"`
	NaturezaOperacao string      `json:"naturezaOperacao"`
	TipoOperacao     string      `json:"tipoOperacao"`
	Finalidade       string      `json:"finalidade"`
	ConsumidorFinal  bool        `json:"consumidorFinal"`
	EnviarPorEmail   bool        `json:"enviarPorEmail"`
	NfeReferida      NfeReferida `json:"nfeReferida"`
	DataEmissao      string      `json:"dataEmissao"`
	Pedido           Pedido      `json:"pedido"`
	Transporte       Transporte  `json:"transporte"`
	Cliente          Cliente     `json:"cliente"`
	Itens            []Itens     `json:"itens"`
}

type Endereco struct {
	Uf          string `json:"uf"`
	Cidade      string `json:"cidade"`
	Logradouro  string `json:"logradouro"`
	Numero      string `json:"numero"`
	Complemento string `json:"complemento"`
	Bairro      string `json:"bairro"`
	Cep         string `json:"cep"`
}

type Csc struct {
	Id     string `json:"id"`
	Codigo string `json:"codigo"`
}

type Ambiente struct {
	SequencialNFe int8 `json:"sequencialNFe"`
	SerieNFe      int8 `json:"serieNFe"`
	Csc           Csc  `json:"csc"`
}

type EmissaoNFeConsumidor struct {
	AmbienteProducao    Ambiente `json:"ambienteProducao"`
	AmbienteHomologacao Ambiente `json:"ambienteHomologacao"`
}

type IncludeNewBusiness struct {
	Id                     string               `json:"id"`
	Cnpj                   string               `json:"cnpj"`
	InscricaoEstadual      string               `json:"inscricaoEstadual"`
	InscricaoMunicipal     string               `json:"inscricaoMunicipal"`
	RazaoSocial            string               `json:"razaoSocial"`
	NomeFantasia           string               `json:"nomeFantasia"`
	OptanteSimplesNacional bool                 `json:"optanteSimplesNacional"`
	Email                  string               `json:"email"`
	TelefoneComercial      string               `json:"telefoneComercial"`
	Endereco               Endereco             `json:"endereco"`
	EmissaoNFeConsumidor   EmissaoNFeConsumidor `json:"emissaoNFeConsumidor"`
}
