// PostgreSQL table configuration
// supplier BIGSERIAL PRIMARY KEY NOT NULL,
// active BOOLEAN NOT NULL,
// sensitive_data JSON,
// administrative JSON,
// address JSON,
// contact JSON,
// shipping JSON,
// categories JSON,
// employees JSON,
// service_week JSON,
// service_schedule JSON,
// delivery_week JSON,
// delivery_schedule JSON,
// settings JSON

// supplier PRIMARY KEY NOT NULL, active BOOLEAN NOT NULL, sensitive_data JSON, administrative JSON, address JSON, contact JSON, shipping JSON, categories JSON, employees JSON, service_week JSON, service_schedule JSON, delivery_week JSON, delivery_schedule JSON, settings JSON

// Retrieves the latest added supplier (serial) from the table suppliers
// SELECT currval(pg_get_serial_sequence("suppliers", 'supplier"));

package account

import (
	// Standard packages
	"context"
	"strings"
	"time"

	// Remote packages
	"github.com/golang-jwt/jwt/v4"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	
	// Local packages
	"api/helpers/functions/logger"
	pg "api/database/postgres/connection"
	"api/database/models/users"
	"api/models/response"
)

// NEEDS TO BE REWORKED
func InsertSupplierAccount (c *fiber.Ctx) error {
	// Temporary hardcoded security measure
	if me := c.Params("me"); strings.Compare(me, "suprastellas") != 0 {
		return c.SendStatus(404)
	}

	var details users.Details
	details.CreatedAt = uint64(time.Now().Unix())
	details.LastSession = details.CreatedAt

	var data users.Data
	taxpayer := c.Params("taxpayer")
	passphrase := ""
	alias := c.Params("alias")
	supplier := uuid.New()
	if _, err := pg.Pool.Exec(
		context.Background(),
		`INSERT INTO users ("user", details, active, taxpayer, passphrase, alias, data) VALUES ($1, $2, $3, $4, $5, $6, $7)`,
		supplier, details, true, taxpayer, passphrase, alias, data)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	return c.SendStatus(200)
}

func SelectSupplierAccount (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)
	// Executes the database query
	var account users.SupplierAccount
	err := pg.Pool.QueryRow(
		context.Background(),
		`SELECT
			data, employees, settings, membership
		FROM
			users
		WHERE
			"user" = $1`,
		user).Scan(&account.Data, &account.Employees, &account.Settings, &account.Membership)
	if err != nil {
		logger.Error.Println(err)
		var r response.Default
		r.Error = "ba0"
		return c.JSON(r)
	}
	// Returns the retrieved data
	return c.JSON(account)
}

func UpdateSupplierAccountData (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)
	// Parses the body data
	var data users.Data // This struct stores the entire JSON

	
	if err := c.BodyParser(&data); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// Executes the database query
	if _, err := pg.Pool.Exec(
		context.Background(),
		`UPDATE users SET data = $1 WHERE "user" = $2`,
		data, user)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	// Returns a success status
	return c.SendStatus(200)
}

func UpdateSupplierAccountSettings (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)
	// Parses the body data
	var settings users.Settings // This struct stores the entire JSON
	if err := c.BodyParser(&settings); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	// Executes the database query
	if _, err := pg.Pool.Exec(
		context.Background(),
		`UPDATE users SET settings = $1 WHERE "user" = $2`,
		settings, user)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	// Returns a success status
	return c.SendStatus(200)
}

func UpdateSupplierAccountEmployees (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}

	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	user := claims["user"].(string)
	// Parses the body data struct Employees
	var employees users.EmployeesRegister // This struct stores the entire JSON
	if err := c.BodyParser(&employees); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	// Executes the database query
	if _, err := pg.Pool.Exec(
		context.Background(),
		`UPDATE users SET employees = $1 WHERE "user" = $2`,
		employees, user)
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	// Returns a success status
	return c.SendStatus(200)
}