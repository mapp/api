// GET   retrieve any resource(s) (only method allowed to have urlencoded params)
// POST  to create a new resource
// PUT   replaces the whole resource (preferable usage when there are no further consequences to any of the updated fields. eg: replacing whole "data")
// PATCH applies partial modifications to a resource (preferable for modifying specific fields requiring further modifications in other resources)

package router

import (
	// Remote packages
	"github.com/gofiber/fiber/v2"

	// Local packages
	appEvents "api/endpoints/app/events"
	appPublic "api/endpoints/app/public"

	usersRegistration "api/endpoints/users/registration"

	supplierAccount       "api/endpoints/supplier/account"
	supplierAuthorization "api/endpoints/supplier/authorization"
	supplierItems         "api/endpoints/supplier/items"
	supplierOrders        "api/endpoints/supplier/orders"
	supplierPassphrase    "api/endpoints/supplier/passphrase"

	// supplierCarriers      "api/endpoints/supplier/carriers"
	supplierCarriers      "api/endpoints/supplier/carriers"
	supplierRelationships "api/endpoints/supplier/relationships"
	supplierSellers       "api/endpoints/supplier/sellers"

	customerAuthorization "api/endpoints/customer/authorization"
	// customerPassphrase    "api/endpoints/customer/passphrase"
	customerAccount       "api/endpoints/customer/account"
	customerOrders        "api/endpoints/customer/orders"

	// Enotas
	supplierInvoice "api/endpoints/supplier/invoices"
)

func RoutesHandler(app *fiber.App) {
	// Public routes
	app.Get("/",                         appPublic.Pong) // Server heartbeat endpoint
	app.Get("/store/:alias",             appPublic.SelectPublicStoreDetails)
	app.Get("/store/:alias/item/:entry", appPublic.SelectPublicSupplierItem)
	app.Get("/search/item/:description", appPublic.SelectPublicSupplierItemsFromDescription)

	// Events' routes
	app.Post("/events/prelaunch", appEvents.InsertPrelaunchLead)

	// Supplier routes
	app.Post("/supplier/authorization",         supplierAuthorization.AuthenticateSupplier)
	app.Put("/resource/supplier/authorization", supplierPassphrase.UpdateSupplierPassphrase)

	// app.Post("/supplier/account/:me/:key/:alias",   supplierAccount.InsertSupplierAccount)
	app.Get("/resource/supplier/account",           supplierAccount.SelectSupplierAccount)
	// app.Get("/resource/supplier/account/data/employees/sellers/:identifier",  supplierAccount.SelectSupplierAccountDataEmployeesSeller)
	// app.Get("/resource/supplier/account/data/employees/carriers/:identifier", supplierAccount.SelectSupplierAccountDataEmployeesCarrier)
	app.Put("/resource/supplier/account/data",      supplierAccount.UpdateSupplierAccountData)
	app.Put("/resource/supplier/account/settings",  supplierAccount.UpdateSupplierAccountSettings)
	app.Put("/resource/supplier/account/employees", supplierAccount.UpdateSupplierAccountEmployees)

	app.Get("/resource/supplier/items/total", supplierItems.SelectSupplierItemsTotal)

	app.Post("/resource/supplier/item",                        supplierItems.InsertSupplierItem)
	app.Get("/resource/supplier/item/:entry",                  supplierItems.SelectSupplierItem)
	app.Get("/resource/supplier/items",                        supplierItems.SelectSupplierItems)
	app.Put("/resource/supplier/item/data",                    supplierItems.UpdateSupplierItemData)
	app.Put("/resource/supplier/item/activity",                supplierItems.UpdateSupplierItemActivity)
	app.Patch("/resource/supplier/item/data/groups/category",  supplierItems.UpdateSupplierItemDataGroupsCategory)
	app.Patch("/resource/supplier/item/data/control/quantity", supplierItems.UpdateSupplierItemDataControlQuantity)
	// app.Delete("/resource/supplier/item/:entry/data/media/image/:element", supplierItems.DeleteSupplierItemDataMediaImage)

	app.Get("/resource/supplier/orders",                        supplierOrders.SelectSupplierOrders)
	app.Get("/resource/supplier/order/:record",                 supplierOrders.SelectSupplierOrder)
	app.Put("/resource/supplier/order/data",                    supplierOrders.UpdateSupplierOrderData)
	app.Post("/resource/supplier/order/signature",              supplierOrders.InsertSupplierOrderSignature)
	app.Post("/resource/supplier/order/:record/invoice/:model", supplierOrders.InsertSupplierOrderInvoice)
	// app.Put("/resource/supplier/order/:record/invoice",         supplierOrders.UpdateSupplierOrderInvoice)

	// Carriers routes
	app.Post("/carrier/authorization", supplierCarriers.AuthenticateCarrier) // and order
	app.Put("/carrier/authorization",  supplierCarriers.UpdateCarrierPassphrase)
	app.Post("/carrier/verify/phone",  supplierCarriers.VerifyUserPhoneByTaxpayer)
	// BELOW ARE BETA ROUTES
	app.Get("/resource/carrier/orders", supplierCarriers.SelectCarrierOrders)
	app.Put("/resource/carrier/order",  supplierCarriers.UpdateCarrierOrder)
	// app.Put("/api/res/business/:supplier/items/:item", items.Update)
	// app.Delete("/api/res/business/:supplier/items/:item", items.Delete)

	// Relationship routes
	app.Get("/resource/supplier/relationships",                      supplierRelationships.SelectRelationships)
	app.Get("/supplier/relationships/:identifier",                   supplierRelationships.SelectRelationshipsTaxpayer)
	app.Post("/resource/supplier/relationships/:identifier",         supplierRelationships.InsertExistedUserAsRelationship)
	app.Post("/resource/supplier/relationships/insert/payables" ,    supplierRelationships.InsertRelationshipPayables)
	app.Post("/resource/supplier/relationships/insert/receivables" , supplierRelationships.InsertRelationshipReceivables)
	app.Delete("/resource/supplier/relationships/payables" ,    supplierRelationships.DeleteRelationshipPayables)
	app.Delete("/resource/supplier/relationships/receivables" , supplierRelationships.DeleteRelationshipReceivables)
	app.Put("/resource/supplier/relationships/update",               supplierRelationships.UpdateRelationship)

	// Sellers routes
	app.Post("/seller/authorization",                                                supplierSellers.AuthenticateSeller)
	app.Put("/seller/authorization",                                                 supplierSellers.UpdateSellerPassphrase)
	app.Post("/seller/verify/phone",                                                 supplierSellers.VerifyUserPhoneByTaxpayer)
	app.Get("/resource/seller/customer/:identifierField/:identifierValue/addresses", supplierSellers.SelectCustomerAddresses) // Uses taxpayer or alias field to retrieve any customer addresses
	app.Get("/resource/seller/sales",                                                supplierSellers.SelectSellerSales)
	app.Get("/resource/seller/supplier/carriers",                                    supplierSellers.SelectSupplierCarriers)


	// Employees
	app.Get("/employees/:identifier/:alias", supplierCarriers.SelectExistsCarrierOrSeller)

	// Customers routes
	app.Post("/customer/authorization", customerAuthorization.AuthenticateCustomer)
	// app.Put("/resource/customer/authorization", customerPassphrase.UpdateCustomerPassphrase)

	app.Get("/resource/customer/account", customerAccount.SelectCustomerAccount)
	app.Put("/resource/customer/account", customerAccount.UpdateCustomerAccount)
	app.Post("/resource/customer/order/store/:alias", customerOrders.InsertCustomerOrder)
	app.Get("/resource/customer/orders", customerOrders.SelectCustomerOrders)

	// Users routes
	app.Post("/taxpayer/phone/code/transmission",                        usersRegistration.SendCodeToUserTaxpayerPhone)       // To a registered taxpayer if found in the database
	app.Post("/phone/code/transmission",                                 usersRegistration.SendCodeToPhone)                   // To any user
	app.Post("/user/activated",                                          usersRegistration.InsertActivatedUser)               // Inserts a new active user into the database
	app.Post("/resource/user/unactivated",                               usersRegistration.InsertUnactivatedUser)             // Inserts a new unactive user into the database
	app.Get("/user/:identifierField/:identifierValue/activity-and-type", usersRegistration.SelectExistsUser)                  // Checks if the given taxpayer or alias is already registered
	app.Get("/user/unactivated/taxpayer/:taxpayer/data",                 usersRegistration.SelectUnactivatedUserTaxpayerData) // Retrieves disclosable fields from an unactive user (to autofill form fields and complete registration)
	app.Get("/resource/user",                                            usersRegistration.SelectUser)                        // Retrieves disclosable fields from an active user (usually for refreshing purposes)
	app.Get("/user/unactivated/taxpayer/:taxpayer/data-and-background",  usersRegistration.SelectUnactivatedUser)             // Retrieves data and backgroud from an unactive user
	app.Put("/user/passphrase/update",                                   usersRegistration.UpdateUserPassphrase)              // Updates the user passphrase
	//Enotas
	app.Post("/resource/supplier/invoice/businessid",              supplierInvoice.GenerateBusinessId ) //Includes new business into enotas db and update users data to include businessId
	app.Post("/resource/supplier/invoice/:businessid/certificate", supplierInvoice.LinkCertificate)     //links digital certificate with bussinessID in E-Notas
	app.Post("/resource/supplier/invoice/:businessid/logo",        supplierInvoice.LinkLogo)			//links logotipo with bussinessID in E-Notas
	app.Get("/resource/supplier/invoice/searchNfe/:nfeid",         supplierInvoice.SearchNfe)           //search NFE in E-Notas
}