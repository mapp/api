// PostgreSQL table configuration
// customer BIGSERIAL PRIMARY KEY NOT NULL,
// active BOOLEAN NOT NULL,
// sensitive_data JSON,
// administrative JSON,
// address JSON,
// contact JSON,
// shipping JSON,
// categories JSON,
// employees JSON,
// service_week JSON,
// service_schedule JSON,
// delivery_week JSON,
// delivery_schedule JSON,
// settings JSON

// customer BIGSERIAL PRIMARY KEY NOT NULL, active BOOLEAN NOT NULL, sensitive_data JSON, administrative JSON, address JSON, contact JSON, shipping JSON, categories JSON, employees JSON, service_week JSON, service_schedule JSON, delivery_week JSON, delivery_schedule JSON, settings JSON

// Minified table configuration
// customer BIGSERIAL PRIMARY KEY NOT NULL, created_at TEXT NOT NULL, active BOOLEAN NOT NULL, key TEXT NOT NULL, passphrase TEXT NOT NULL, data JSON

// Retrieves the latest added customer (serial) from the table customers
// SELECT currval(pg_get_serial_sequence("customers", 'customer"));

package passphrase

import (
	// Standard packages
	"context"

	// Remote packages
	"github.com/golang-jwt/jwt/v4"
	"github.com/gofiber/fiber/v2"
	"github.com/alexedwards/argon2id"
	
	// Local packages
	"api/helpers/functions/logger"
	pg "api/database/postgres/connection"
)

func UpdateUserPassphraseUnlogged (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	customer := claims["user"].(string)

	var passphrase string
	if err := c.BodyParser(&passphrase); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	hash, err := argon2id.CreateHash(passphrase, argon2id.DefaultParams)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	if _, err := pg.Pool.Exec(
		context.Background(),
		"UPDATE customers SET passphrase = $1 WHERE "user" = $2",
		hash, customer);
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	return c.SendStatus(200)
}

func UpdateUserPassphraseLogged (c *fiber.Ctx) error {
	// Parse attempt being nil causes panic
	if c.Locals("authorization") == nil {
		return c.SendStatus(500)
	}
	// Dumps the content from the JWT token
	token := c.Locals("authorization").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	customer := claims["user"].(string)

	var passphrase string
	if err := c.BodyParser(&passphrase); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	hash, err := argon2id.CreateHash(passphrase, argon2id.DefaultParams)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	if _, err := pg.Pool.Exec(
		context.Background(),
		"UPDATE customers SET passphrase = $1 WHERE "user" = $2",
		hash, customer);
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	return c.SendStatus(200)
}