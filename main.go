package main

import (
	// Standard packages
	"os"
	// "time"
	// "io/ioutil"

	// Remote packages
	// "github.com/gofiber/fiber/v2" // Legacy
	// "github.com/gofiber/jwt" // Legacy
	// "github.com/gofiber/cors" // Legacy
	// "github.com/gofiber/recover" // Legacy

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/recover"
	// "github.com/gofiber/fiber/v2/middleware/limiter"
	"github.com/gofiber/helmet/v2"
	jwtware "github.com/gofiber/jwt/v3"
	// "github.com/golang-jwt/jwt/v4"

	// Local packages
	env "api/helpers/variables/environment"
	pg "api/database/postgres/connection"
	pgRoutines "api/database/postgres/routines"
	redis "api/database/redis/connection"
	"api/helpers/functions/logger"
	"api/router"
)

func main() {
	// Initialize logging
    logger.InitializeLogger(
    	// ioutil.Discard,
    	os.Stdout,
    	os.Stdout,
    	os.Stderr)

	// Initialize Postgres database connection
	pg.InitializeDatabaseConnection()
	defer pg.Pool.Close()

	// Run Postgres database startup routines
	pgRoutines.EnsureTables()

	// Test connection to Redis database
	redis.PingDatabase()

	// Declare and initialize app
	app := fiber.New()

	// Panic recover middleware
	app.Use(recover.New())

	// CORS middleware
	app.Use(cors.New())

	// Helment middleware
	app.Use(helmet.New())

	// Limiter middleware
	// app.Use(limiter.New())

	// Or extend your config for customization
	// app.Use(limiter.New(limiter.Config{
	//     Next: func(c *fiber.Ctx) bool {
	//         return c.IP() == "127.0.0.1"
	//     },
	//     Max:          30,
	//     Expiration:   20 * time.Second,
	//     KeyGenerator: func(c *fiber.Ctx) string {
	//         return c.Get("x-forwarded-for")
	//     },
	// }))

	// JWT middleware
	// Authorization (Bearer) header on routes starting with "/resource"
	app.Use("/resource", jwtware.New(jwtware.Config{
		SigningKey: env.JwtSigningKey,
		ContextKey: "authorization",
	}))

	// Groups all routes under the same function
	router.RoutesHandler(app)
	app.Listen(env.ApiPort)
}