package hasher

import (
	"hash/crc32"
	"crypto/sha1"
	"encoding/hex"
)

func GetCrc32Sum (bs []byte) uint32 {
	h := crc32.NewIEEE()
	h.Write(bs)
	return h.Sum32()
}

func GetSha1SumHex (bs []byte) string {
	h := sha1.New()
	h.Write(bs)
	return hex.EncodeToString(h.Sum(nil))
}
