// Minified table configuration
// customer BIGSERIAL PRIMARY KEY NOT NULL, details JSONB NOT NULL, active BOOLEAN NOT NULL, key JSONB NOT NULL UNIQUE, passphrase TEXT NOT NULL, data JSON

package authorization

import (
	// Standard packages
	"context"
	"time"

	// Remote packages
	"github.com/alexedwards/argon2id"
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"

	// Local packages
	"api/database/models/users"
	pg "api/database/postgres/connection"
	"api/helpers/functions/logger"
	"api/helpers/functions/sifter"
	env "api/helpers/variables/environment"
	// "api/helpers/functions/concat"
)

func AuthenticateCustomer(c *fiber.Ctx) error {
	var access users.AuthorizationRequest // This struct stores the entire JSON
	if err := c.BodyParser(&access); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	// fmt.Printf("%v\n", access)

	updatedAt := uint64(time.Now().Unix()) // Updates the last session property at the details column
	// mobile := concat.Join(access.Phone.Prefix, access.Phone.Destination, access.Phone.Subscriber)

	var customer, hash string
	// var key users.E123Phone
	var data users.Data

	err := pg.Pool.QueryRow(
		context.Background(),
		`UPDATE
			customers
		SET
			updated_at = $1
		WHERE
			taxpayer = $2
		RETURNING
			customer, passphrase, data`,
		updatedAt, access.Taxpayer).Scan(&customer, &hash, &data)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(401)
	}

	// Empty array initialization
	if data.Addresses == nil {
		data.Addresses = []users.Address{}
	}

	match, err := argon2id.ComparePasswordAndHash(access.Passphrase, hash)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	if match {
		// Create token
		token := jwt.New(jwt.SigningMethodHS256)
		// Set claims
		claims := token.Claims.(jwt.MapClaims)
		claims["user"] = customer
		claims["exp"] = time.Now().Add(time.Hour * 72).Unix()
		// Generate encoded token and send it as response
		encodedToken, err := token.SignedString(env.JwtSigningKey)
		if err != nil {
			logger.Error.Println(err)
			return c.SendStatus(500)
		}

		var sd users.AuthorizationResponse
		sd.Token = &encodedToken
		sd.Status = sifter.SetCustomerStatusOnKey(data.Administrative.Reference.Phone)

		if sd.Status == 1 { // 0 -> Ok, 1 -> Not allowed in beta
			return c.JSON(sd)
		}

		sd.Payload = &data
		return c.JSON(sd)
	} else {
		return c.SendStatus(404)
	}
}

// "INSERT INTO users (details, active, alias, passphrase) VALUES ($1, $2, $3, $4) ON CONFLICT (key) DO UPDATE customers SET details = jsonb_set(details, '{lastSession}', to_jsonb($5::text)) WHERE alias = $6 RETURNING customer, passphrase, data"
