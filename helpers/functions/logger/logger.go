package logger

import (
    // Standard packages
    "io"
    "log"
)

var (
    // Trace   *log.Logger
    Info    *log.Logger
    Warning *log.Logger
    Error   *log.Logger
)

func InitializeLogger(
    // traceHandle io.Writer,
    infoHandle io.Writer,
    warningHandle io.Writer,
    errorHandle io.Writer) {

    // Trace = log.New(traceHandle,
    //     "TRACE: ",
    //     log.Ldate|log.Ltime|log.Lshortfile)

    Info = log.New(infoHandle,
        "INFO: ",
        log.Ldate|log.Ltime|log.Llongfile)

    Warning = log.New(warningHandle,
        "WARNING: ",
        log.Ldate|log.Ltime|log.Llongfile)

    Error = log.New(errorHandle,
        "ERROR: ",
        log.Ldate|log.Ltime|log.Llongfile)
}

    // Trace.Println("I have something standard to say")
    // Info.Println("Special Information")
    // Warning.Println("There is something you need to know about")
    // Error.Println("Something has failed")