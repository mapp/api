// PostgreSQL table configuration
// supplier BIGSERIAL PRIMARY KEY NOT NULL,
// active BOOLEAN NOT NULL,
// sensitive_data JSON,
// administrative JSON,
// address JSON,
// contact JSON,
// shipping JSON,
// categories JSON,
// employees JSON,
// service_week JSON,
// service_schedule JSON,
// delivery_week JSON,
// delivery_schedule JSON,
// settings JSON

// supplier BIGSERIAL PRIMARY KEY NOT NULL, active BOOLEAN NOT NULL, sensitive_data JSON, administrative JSON, address JSON, contact JSON, shipping JSON, categories JSON, employees JSON, service_week JSON, service_schedule JSON, delivery_week JSON, delivery_schedule JSON, settings JSON

// Minified table configuration
// supplier BIGSERIAL PRIMARY KEY NOT NULL, created_at TEXT NOT NULL, active BOOLEAN NOT NULL, key TEXT NOT NULL, passphrase TEXT NOT NULL, data JSON

// Retrieves the latest added supplier (serial) from the table suppliers
// SELECT currval(pg_get_serial_sequence("suppliers", 'supplier"));

package business

import (
	// Standard packages
	"context"
	"strings"
	"time"
	"strconv"
	"hash/crc32"
	"encoding/binary"

	// Remote packages
	"github.com/golang-jwt/jwt/v4"
	"github.com/gofiber/fiber/v2"
	"github.com/alexedwards/argon2id"
	
	// Local packages
	env "api/helpers/variables/environment"
	"api/helpers/functions/logger"
	pg "api/database/postgres/connection"
	"api/models/response"
)

func ComposeKey (integer uint32) string {
	crc32q := crc32.MakeTable(crc32.IEEE) // Uint32 from IEEE polynomial
	ba := make([]byte, 4)
    binary.LittleEndian.PutUint32(ba, integer) // uint32 to byte array
	result := crc32.Checksum([]byte(ba), crc32q) // produces the CRC32
	key := strconv.FormatUint(uint64(result), 16) // converts from uint32 to string hexa
	return key
}

func AuthenticateSupplier (c *fiber.Ctx) error {
	var access users.AuthorizationRequest // This struct stores the entire JSON
	if err := c.BodyParser(&access); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	var supplier int64
	var hash string
	var data users.Data
	err := pg.Pool.QueryRow(
		context.Background(),
		"SELECT user, passphrase, data FROM users WHERE alias = $1",
		&access.Alias).Scan(&user, &hash, &data);
	if err != nil {
		logger.Error.Println(err)
		var r response.Default
		r.Error = "ba0"
		return c.JSON(r)
	}
	// If hash is empty it means the supplier is new and should set a new passphrase
	// (At the "Account" page)
	if hash == "" {
		c.Send("Green")
	}

	match, err := argon2id.ComparePasswordAndHash(access.Passphrase, hash)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	if match {
		// Create token
		token := jwt.New(jwt.SigningMethodHS256)
		// Set claims
		claims := token.Claims.(jwt.MapClaims)
		claims["accessKey"] = access.Alias
		claims["exp"] = time.Now().Add(time.Hour * 72).Unix()
		// Generate encoded token and send it as response
		encodedToken, err := token.SignedString(env.JwtSigningKey)
		if err != nil {
			logger.Error.Println(err)
			return c.SendStatus(500)
		}

		var sd users.SupplierSignInData
		sd.Token = &encodedToken
		sd.Data = &data
		return c.JSON(sd)
	} else {
		var r response.Default
		r.Error = "ba1"
		return c.JSON(r)
	}
}

func UpdateSupplierPassphrase (c *fiber.Ctx) error {
	var access users.AuthorizationRequest // This struct stores the entire JSON
	if err := c.BodyParser(&access); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	hash, err := argon2id.CreateHash(access.Passphrase, argon2id.DefaultParams)
	if err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	if _, err := pg.Pool.Exec(
		context.Background(),
		"UPDATE users SET passphrase = $1 WHERE alias = $2",
		hash, &access.Alias);
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	return c.SendStatus(200)
}

func InsertSupplierData (c *fiber.Ctx) error {
	// Temporary hardcoded security measure
	if me := c.Params("me"); strings.Compare(me, "moveredeinceps") != 0 {
		return c.SendStatus(404)
	}

	timestamp := time.Now().Unix()
	key := ComposeKey(uint32(timestamp))
	if _, err := pg.Pool.Exec(
		context.Background(),
		"INSERT INTO users (created_at, active, alias, passphrase) VALUES ($1, $2, $3, $4)",
		timestamp, true, alias, "");
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	return c.SendStatus(200)
}

func SelectSupplierData (c *fiber.Ctx) error {
	c.Send("Hello, World!")
}

func UpdateSupplierData (c *fiber.Ctx) error {
	supplier := c.Params("supplier")

	var data users.Data // This struct stores the entire JSON
	if err := c.BodyParser(&data); err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}

	if _, err := pg.Pool.Exec(
		context.Background(),
		"UPDATE users SET data = $1 WHERE "user" = $2",
		data, user);
	err != nil {
		logger.Error.Println(err)
		return c.SendStatus(400)
	}
	return c.SendStatus(200)
}

func InsertItem (c *fiber.Ctx) error {
	c.Send("Hello, World!")
}

func UpdateItem (c *fiber.Ctx) error {
	c.Send("Hello, World!")
}

func DeleteItem (c *fiber.Ctx) error {
	c.Send("Hello, World!")
}